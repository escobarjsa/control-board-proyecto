﻿/*------------------------------------------------------------------------
NOMBRE: [Integracion_Continua]
REQUERIMIENTO: ControlBoard
PROPOSITO: Insertar datos en la tabla [dbo].[Integracion_Continua]
FECHA DE CREACION: 29-03-2022
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

CREATE TABLE [dbo].[Integracion_Continua] (
    [id]            INT            IDENTITY (1, 1) NOT NULL,
    [guid_proyecto] NVARCHAR (MAX) NULL,
    [id_ci]         NVARCHAR (MAX) NULL,
    [inicio]        DATETIME       NULL,
    [fin]           DATETIME       NULL,
    [pipeline]      NVARCHAR (MAX) NULL,
    [build_number]  NVARCHAR (MAX) NULL,
    [estado]        NVARCHAR (MAX) NULL,
    [encolado]      NVARCHAR (MAX) NULL
);


﻿/*------------------------------------------------------------------------
NOMBRE: Proyecto
REQUERIMIENTO: ControlBoard
PROPOSITO: tabla [dbo].[Proyecto]
FECHA DE CREACION: 29-03-2022
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

CREATE TABLE [dbo].[Proyecto] (
    [id]            INT            IDENTITY (1, 1) NOT NULL,
    [nombre]        NVARCHAR (MAX) NULL,
    [descripcion]   NVARCHAR (MAX) NULL,
    [coleccion]     NVARCHAR (MAX) NULL,
    [lider_tecnico] NVARCHAR (MAX) NULL,
    [guid]          VARCHAR (300)  NOT NULL,
    [createdOn]     DATETIME       NULL,
    CONSTRAINT [PK_Proyecto] PRIMARY KEY CLUSTERED ([guid] ASC)
);


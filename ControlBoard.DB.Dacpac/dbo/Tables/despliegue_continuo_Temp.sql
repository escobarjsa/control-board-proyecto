﻿/*------------------------------------------------------------------------
NOMBRE: despliegue_continuo_Temp
REQUERIMIENTO: ControlBoard
PROPOSITO: Insertar datos en la tabla [dbo].[despliegue_continuo_Temp]
FECHA DE CREACION: 29-03-2022
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

CREATE TABLE [dbo].[despliegue_continuo_Temp] (
    [id]             INT            IDENTITY (1, 1) NOT NULL,
    [guid_proyecto]  NVARCHAR (MAX) NULL,
    [id_cd]          NVARCHAR (MAX) NULL,
    [inicio]         DATETIME       NULL,
    [fin]            DATETIME       NULL,
    [pipeline]       NVARCHAR (MAX) NULL,
    [pipeline_build] NVARCHAR (MAX) NULL,
    [ambiente]       NVARCHAR (MAX) NULL,
    [estado]         NVARCHAR (MAX) NULL,
    [encolado]       DATETIME       NULL,
    [release_name]   NVARCHAR (MAX) NULL
);


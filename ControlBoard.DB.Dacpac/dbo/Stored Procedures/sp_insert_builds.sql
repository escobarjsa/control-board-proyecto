﻿/*------------------------------------------------------------------------
NOMBRE: sp_insert_builds
REQUERIMIENTO: ControlBoard
PROPOSITO: Insertar datos en la tabla [dbo].[Integracion_Continua]
FECHA DE CREACION: 29-03-2022
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

CREATE PROCEDURE [dbo].[sp_insert_builds] ( 
 @guid_proyecto nvarchar(4000),
 @id_ci nvarchar(4000),
 @inicio datetime,
 @fin datetime,
 @pipeline nvarchar(4000),
 @build_number nvarchar(4000),
 @estado nvarchar(4000),
 @encolado nvarchar(4000)
)  
AS  
 
SET NOCOUNT ON;  
DECLARE @s_Error VARCHAR(500);
DECLARE @sEspacioBlanco VARCHAR(2); 
BEGIN
    SET NOCOUNT ON;
    
    BEGIN TRY
        -- Verificar si ya existe el registro para evitar duplicados
        IF NOT EXISTS (SELECT 1 FROM [dbo].[Integracion_Continua] 
                       WHERE guid_proyecto = @guid_proyecto AND id_ci = @id_ci)
        BEGIN
            INSERT INTO [dbo].[Integracion_Continua]
            ([guid_proyecto], [id_ci], [inicio], [fin], [pipeline], [build_number], [estado], [encolado])
            VALUES
            (@guid_proyecto, @id_ci, @inicio, @fin, @pipeline, @build_number, @estado, @encolado);
        END
    END TRY
    BEGIN CATCH
        -- Captura el error y lo maneja
        SET @s_Error = ERROR_MESSAGE();
        RAISERROR (@s_Error, 16, 1);
    END CATCH
END
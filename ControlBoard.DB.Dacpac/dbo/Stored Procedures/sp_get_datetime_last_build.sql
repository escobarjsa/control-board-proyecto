﻿/*------------------------------------------------------------------------
NOMBRE: sp_get_datetime_last_build
REQUERIMIENTO: ControlBoard
PROPOSITO: consultar datos en la tabla [dbo].[sp_get_datetime_last_build]
FECHA DE CREACION: 29-03-2022
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

CREATE PROCEDURE [dbo].[sp_get_datetime_last_build] ( 

 @id nvarchar(4000)
)  
AS  
 
SET NOCOUNT ON;  
DECLARE @s_Error VARCHAR(500);
DECLARE @sEspacioBlanco VARCHAR(2); 
BEGIN TRY  

 SET @s_Error = 'Error: Consultando Integración continua';
 SET @sEspacioBlanco = '';
	SELECT TOP 1 * FROM [dbo].[Integracion_Continua] AS ic WHERE ic.guid_proyecto = @id; 
END TRY  
BEGIN CATCH  
SET @s_error =ERROR_MESSAGE()  
 RAISERROR ( @s_error ,16 ,1 )  
END CATCH
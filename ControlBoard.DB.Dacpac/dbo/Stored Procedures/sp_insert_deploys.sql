﻿/*------------------------------------------------------------------------
NOMBRE: sp_insert_deploys
REQUERIMIENTO: ControlBoard
PROPOSITO: Insertar datos en la tabla [dbo].[sp_insert_deploys]
FECHA DE CREACION: 29-03-2022
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

CREATE PROCEDURE [dbo].[sp_insert_deploys]
(
 @guid_proyecto nvarchar (4000),
 @id_cd nvarchar (4000),
 @inicio datetime,
 @fin datetime,
 @pipeline nvarchar (4000),
 @ambiente nvarchar (4000),
 @estado nvarchar (4000),
 @encolado datetime,
 @release_name nvarchar (4000)
)
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        -- Comprueba si ya existe un registro con el mismo id_cd y guid_proyecto
        IF NOT EXISTS (SELECT 1 FROM [dbo].[despliegue_continuo]
                       WHERE id_cd = @id_cd AND guid_proyecto = @guid_proyecto)
        BEGIN
            INSERT INTO [dbo].[despliegue_continuo]
            ([guid_proyecto],
             [id_cd],
             [inicio],
             [fin],
             [pipeline],
             [ambiente],
             [estado],
             [encolado],
             [release_name])
            VALUES
            (@guid_proyecto,
             @id_cd,
             @inicio,
             @fin,
             @pipeline,
             @ambiente,
             @estado,
             @encolado,
             @release_name);
        END
    END TRY
    BEGIN CATCH
        -- Maneja el error
        DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
        RAISERROR(@ErrorMessage, 16, 1);
    END CATCH
END

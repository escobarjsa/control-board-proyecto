﻿
/*------------------------------------------------------------------------
NOMBRE: sp_destructive_reset.sql
REQUERIMIENTO: ControlBoard
PROPOSITO: Este procedimiento almacenado es extremadamente destructivo y está diseñado para eliminar todas las restricciones de clave foránea, eliminar todas las tablas, y eliminar todos los procedimientos almacenados en la base de datos.
FECHA DE CREACION: 11-11-2023
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

-- Creando el procedimiento almacenado
CREATE PROCEDURE [dbo].[sp_destructive_reset]

AS
BEGIN
    -- Desactiva el conteo de filas afectadas para evitar mensajes adicionales durante la ejecución
    SET NOCOUNT ON;

    -- Inicializa la variable @sql para construir las instrucciones SQL dinámicamente
    DECLARE @sql NVARCHAR(MAX) = '';

    -- Construye la instrucción SQL para deshabilitar todas las restricciones de clave foránea en todas las tablas
    SELECT @sql += 'ALTER TABLE ' + QUOTENAME(s.name) + '.' + QUOTENAME(t.name) + ' NOCHECK CONSTRAINT ALL; '
    FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id;

    -- Ejecuta la instrucción SQL dinámica para deshabilitar las restricciones
    EXEC sp_executesql @sql;

    -- Reinicia la variable @sql para construir la siguiente instrucción SQL
    SET @sql = '';

    -- Construye la instrucción SQL para eliminar todas las tablas en la base de datos
    SELECT @sql += 'DROP TABLE IF EXISTS ' + QUOTENAME(s.name) + '.' + QUOTENAME(t.name) + '; '
    FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id;

    -- Ejecuta la instrucción SQL dinámica para eliminar todas las tablas
    EXEC sp_executesql @sql;

    -- Declara una variable para almacenar los nombres de los procedimientos almacenados
    DECLARE @nombre nvarchar(500);

    -- Inicializa un cursor para iterar sobre todos los procedimientos almacenados en el esquema dbo
    DECLARE cursor_sp CURSOR FOR
        SELECT [name]
        FROM sys.objects
        WHERE type = 'P' AND schema_id = SCHEMA_ID('dbo');

    -- Abre el cursor
    OPEN cursor_sp;

    -- Obtiene el primer nombre de procedimiento almacenado del cursor
    FETCH NEXT FROM cursor_sp INTO @nombre;

    -- Itera a través de todos los procedimientos almacenados
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Elimina el procedimiento almacenado si existe
        EXEC('DROP PROCEDURE IF EXISTS dbo.[' + @nombre + ']');

        -- Avanza al siguiente nombre de procedimiento almacenado en el cursor
        FETCH NEXT FROM cursor_sp INTO @nombre;
    END;

    -- Cierra el cursor
    CLOSE cursor_sp;

    -- Libera los recursos del cursor
    DEALLOCATE cursor_sp;
END;

﻿/*------------------------------------------------------------------------
NOMBRE: sp_insert_deploys_temp
REQUERIMIENTO: ControlBoard
PROPOSITO: Insertar datos en la tabla [dbo].[sp_insert_deploys_temp]
FECHA DE CREACION: 29-03-2022
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

CREATE PROCEDURE [dbo].[sp_insert_deploys_temp]
(
 @guid_proyecto nvarchar (4000),
 @id_cd nvarchar (4000),
 @inicio datetime,
 @fin datetime,
 @pipeline nvarchar (4000),
 @pipeline_build nvarchar (4000),
 @ambiente nvarchar (4000),
 @estado nvarchar (4000),
 @encolado datetime,
 @release_name nvarchar (4000)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	-- Insert statements for procedure here
		INSERT INTO [dbo].[despliegue_continuo_Temp]
		([guid_proyecto],
			[id_cd],
			[inicio],
			[fin],
			[pipeline],
			[pipeline_build],
			[ambiente],
			[estado],
			[encolado],
			[release_name])
		VALUES
			(@guid_proyecto,
			@id_cd,
			@inicio,
			@fin,
			@pipeline,
			@pipeline_build,
			@ambiente,
			@estado,
			@encolado,
			@release_name)
END

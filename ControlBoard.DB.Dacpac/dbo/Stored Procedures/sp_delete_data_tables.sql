﻿/*------------------------------------------------------------------------
NOMBRE: sp_delete_data_tables.sql
REQUERIMIENTO: ControlBoard
PROPOSITO: Eliminar los datos de las tablas dbo.Proyecto, dbo.Integracion_Continua, dbo. despliegue_continuo_Temp, dbo.despliegue_continuo
FECHA DE CREACION: 11-11-2023
FECHA DE MODIFICACION: 
-------------------------------------------------------------------------*/

CREATE PROCEDURE [dbo].[sp_delete_data_tables]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        BEGIN TRANSACTION;

        DELETE FROM [dbo].[Proyecto];
        DELETE FROM [dbo].[Integracion_Continua];
        DELETE FROM [dbo].[despliegue_continuo_Temp];
        DELETE FROM [dbo].[despliegue_continuo];

        COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;
        DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int;
        SELECT @ErrMsg = ERROR_MESSAGE(), @ErrSeverity = ERROR_SEVERITY();
        RAISERROR(@ErrMsg, @ErrSeverity, 1);
    END CATCH
END

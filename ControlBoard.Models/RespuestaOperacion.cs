﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlBoard.Models
{
    public class RespuestaOperacion
    {
        public List<string> Observaciones { get; set; } = new List<string>();

        public int Estado { get; set; } = 200;

        public string Aprobador { get; set; }

        public string Proyecto { get; set; }

        public string Coleccion { get; set; }
    }
}

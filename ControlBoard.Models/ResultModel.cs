﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlBoard.Models
{
    public class ResultModel<T>
    {
        public bool IsValid { get; set; }
        public string Messagge { get; set; }
        public T Data { get; set; }
    }
}

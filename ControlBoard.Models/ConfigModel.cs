﻿using System;
using System.Reflection.Metadata.Ecma335;

namespace ControlBoard.Models
{
    public class ConfigModel
    {
        public string Url { get; set; }
        public bool TokenRequired { get; set; }
        public string Usser { get; set; }
        public string Password { get; set; }
        public dynamic Others { get; set; } 
    }
}

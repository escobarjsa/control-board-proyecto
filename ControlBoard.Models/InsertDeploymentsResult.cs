﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlBoard.Models
{
    public class InsertDeploymentsResult
    {
        public List<string> ErrorMessage { get; set; }
        public List<string> SuccessMessage { get; set; }
    }
}

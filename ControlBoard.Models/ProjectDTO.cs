﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ControlBoard.Models
{
    public class ProjectDTO
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string state { get; set; }
        public int revision { get; set; }
        public string visibility { get; set; }
        public string lastUdateTime { get; set; }
    }
}

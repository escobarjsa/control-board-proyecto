﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlBoard.Models
{
    public class CadenasEncontradas
    {
        public string Cadena { get; set; }
        public int Posicion { get; set; }
        public int Coincidencias { get; set; }
        public string Texto { get; set; }
        public string XMLformat { get; set; }
        public List<Enlace> Enlace { get; set; } = new List<Enlace>();
        public bool AplicaLectura { get; set; } = false;
    }

    public class Enlace
    {
        public string Direccion { get; set; }
        public bool Archivo { get; set; } = false;
    }
}

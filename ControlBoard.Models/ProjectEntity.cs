﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace ControlBoard.Models
{
    [Table("Proyecto")]
    public class ProjectEntity
    {
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("nombre")]
        [Required]
        public string Nombre { get; set; }

        [Column("descripcion")]
        public string Descripcion { get; set; }

        [Column("coleccion")]
        [Required]
        public string Coleccion { get; set; }

        [Column("lider_tecnico")]
        public string Lider_tecnico { get; set; }

        [Column("guid")]
        [Key]
        [Required]
        public string Guid { get; set; }

        [Column("createdOn")]
        public DateTime? CreatedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ControlBoard.Models
{
    [Table("Integracion_Continua")]
    public class ICEntity
    {
        [Column("id")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("guid_proyecto")]
        [Required]
        public string Guid { get; set; }

        [Column("id_ci")]
        public string Id_ci { get; set; }

        [Column("inicio")]
        public DateTime Inicio { get; set; }

        [Column("fin")]
        public DateTime Fin { get; set; }

        [Column("pipeline")]
        public string Pipeline { get; set; }

        [Column("build_number")]
        public string Build_number { get; set; }

        [Column("estado")]
        public string Estado { get; set; }

        [Column("encolado")]
        public string Encolado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlBoard.Models
{
    public class EntidadCD
    {
        public string id { get; set; }
        public string queuedOn { get; set; }
        public string completedOn { get; set; }
        public Name releaseDefinition { get; set; }
        public string deploymentStatus { get; set; }
        public Name releaseEnvironment { get; set; }
        public string startedOn { get; set; }
        public ReleaserModel release { get; set; }
    }
    public class Name
    {
        public string name { get; set; }

    }

    public class ReleaserModel
    {
        public string name { get; set; }
        public dynamic artifacts { get; set; }
    }
}

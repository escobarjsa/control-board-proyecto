﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlBoard.Models
{
    public class CredentialsModel
    {
        public string UrlBase { get; set; }
        public string UrlBaseRelease { get; set; }
        public string UrlBaseOriginal { get; set; }
        public string UrlProject { get; set; }
        public string ProjectName { get; set; }
        public string AbsoluteRute { get; set; }
        public string FuddvUrl { get; set; }
        public string Usser { get; set; }
        public string Password { get; set; }
        public bool TokenRequired { get; set; }
        public string Callection { get; set; }
        public string Formatos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlBoard.Models
{
    public class EntidadCI
    {
        public string id { get; set; }
        public string buildNumber { get; set; }
        public string result { get; set; }
        public string queueTime { get; set; }
        public string startTime { get; set; }
        public string finishTime { get; set; }
        public Name definition { get; set; }
    }
}

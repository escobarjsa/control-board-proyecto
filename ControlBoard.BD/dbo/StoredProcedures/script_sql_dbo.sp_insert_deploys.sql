/****** Object:  StoredProcedure [dbo].[sp_insert_deploys]    Script Date: 23/03/2022 4:33:34 p. m. ******/

-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database, sp_insert_deploysv
-- =======================================================

CREATE PROCEDURE [dbo].[sp_insert_deploys]
(
 @guid_proyecto nvarchar (4000),
 @id_cd nvarchar (4000),
 @inicio datetime,
 @fin datetime,
 @pipeline nvarchar (4000),
 @ambiente nvarchar (4000),
 @estado nvarchar (4000),
 @encolado datetime,
 @release_name nvarchar (4000)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	-- Insert statements for procedure here
		INSERT INTO [dbo].[despliegue_continuo]
		([guid_proyecto],
			[id_cd],
			[inicio],
			[fin],
			[pipeline],
			[ambiente],
			[estado],
			[encolado],
			[release_name])
		VALUES
			(@guid_proyecto,
			@id_cd,
			@inicio,
			@fin,
			@pipeline,
			@ambiente,
			@estado,
			@encolado,
			@release_name)
END
GO

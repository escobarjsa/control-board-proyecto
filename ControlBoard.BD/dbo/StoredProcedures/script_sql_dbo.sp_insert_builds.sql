/****** Object:  StoredProcedure [dbo].[sp_insert_builds]    Script Date: 23/03/2022 4:29:57 p. m. ******/

-- =======================================================
-- Create Stored Procedure Template for Azure SQL Database, sp_insert_builds
-- =======================================================

CREATE PROCEDURE [dbo].[sp_insert_builds] ( 
 @guid_proyecto nvarchar(4000),
 @id_ci nvarchar(4000),
 @inicio datetime,
 @fin datetime,
 @pipeline nvarchar(4000),
 @build_number nvarchar(4000),
 @estado nvarchar(4000),
 @encolado nvarchar(4000)
)  
AS  
 
SET NOCOUNT ON;  
DECLARE @s_Error VARCHAR(500);
DECLARE @sEspacioBlanco VARCHAR(2); 
BEGIN TRY  

 SET @s_Error = 'Error: Consultando Integración continua';
 SET @sEspacioBlanco = '';
	INSERT INTO [dbo].[Integracion_Continua]
           ([guid_proyecto]
		   ,[id_ci]
		   ,[inicio]
		   ,[fin]
		   ,[pipeline]
		   ,[build_number]
		   ,[estado]
		   ,[encolado])
     VALUES
           (@guid_proyecto
			 ,@id_ci
			 ,@inicio
			 ,@fin
			 ,@pipeline
			 ,@build_number
			 ,@estado
			 ,@encolado)
END TRY  
BEGIN CATCH  
SET @s_error =ERROR_MESSAGE()  
 RAISERROR ( @s_error ,16 ,1 )  
END CATCH
GO

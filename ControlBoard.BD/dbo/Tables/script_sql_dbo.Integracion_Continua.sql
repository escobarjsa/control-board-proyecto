/****** Object:  Table [dbo].[Integracion_Continua]    Script Date: 23/03/2022 4:18:27 p. m. ******/

CREATE TABLE [dbo].[Integracion_Continua](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[guid_proyecto] [nvarchar](max) NULL,
	[id_ci] [nvarchar](max) NULL,
	[inicio] [datetime] NULL,
	[fin] [datetime] NULL,
	[pipeline] [nvarchar](max) NULL,
	[build_number] [nvarchar](max) NULL,
	[estado] [nvarchar](max) NULL,
	[encolado] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

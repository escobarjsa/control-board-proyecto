/****** Object:  Table [dbo].[despliegue_continuo]    Script Date: 23/03/2022 4:14:44 p. m. ******/

CREATE TABLE [dbo].[despliegue_continuo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[guid_proyecto] [nvarchar](max) NULL,
	[id_cd] [nvarchar](max) NULL,
	[inicio] [datetime] NULL,
	[fin] [datetime] NULL,
	[pipeline] [nvarchar](max) NULL,
	[ambiente] [nvarchar](max) NULL,
	[estado] [nvarchar](max) NULL,
	[encolado] [nvarchar](max) NULL,
	[release_name] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

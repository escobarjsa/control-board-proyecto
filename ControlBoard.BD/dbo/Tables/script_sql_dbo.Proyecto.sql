/****** Object:  Table [dbo].[Proyecto]    Script Date: 23/03/2022 4:20:26 p. m. ******/

CREATE TABLE [dbo].[Proyecto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](max) NULL,
	[descripcion] [nvarchar](max) NULL,
	[coleccion] [nvarchar](max) NULL,
	[lider_tecnico] [nvarchar](max) NULL,
	[guid] [varchar](300) NOT NULL,
	[createdOn] [datetime] NULL,
 CONSTRAINT [PK_Proyecto] PRIMARY KEY CLUSTERED 
(
	[guid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

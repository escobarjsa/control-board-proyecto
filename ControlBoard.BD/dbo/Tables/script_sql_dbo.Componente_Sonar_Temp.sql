/****** Object:  Table [dbo].[Componente_Sonar_Temp]    Script Date: 23/03/2022 4:10:21 p. m. ******/

CREATE TABLE [dbo].[Componente_Sonar_Temp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name_poject_sonar] [nvarchar](max) NULL,
	[guid_project] [varchar](300) NOT NULL,
	[url_info_project] [nvarchar](max) NULL,
	[pipe_line] [nvarchar](max) NULL,
 CONSTRAINT [PK_Componente_Sonar_Temp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

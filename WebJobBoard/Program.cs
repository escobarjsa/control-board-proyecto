﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;
using Microsoft.Extensions.DependencyInjection;

namespace WebJobBoard
{
    class Program
    {
        static async Task Main()
        {
            var builder = new HostBuilder();
            builder.ConfigureWebJobs(b =>
            {
                // b.AddTimers();
                b.AddAzureStorageCoreServices();
            }).ConfigureAppConfiguration((context, config) =>
            {
                config.AddJsonFile("appsettings.json");

            }).ConfigureLogging((context, b) =>
            {
                b.ClearProviders();
                b.AddConsole();
            }).ConfigureServices((context, services) =>
            {
                services.Configure<ILogger>(context.Configuration);
                var serviceProvider = services.BuildServiceProvider();
                var logger = serviceProvider.GetService<ILogger<Functions>>();
                services.AddSingleton(typeof(ILogger), logger);

                services.AddSingleton<Functions>();

            });

            
            var host = builder.Build();

            using (host)
            {
                
                await host.StartAsync();
                var instancia = host.Services.GetRequiredService<Functions>();
                instancia.Run();
            }
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace WebJobBoard
{
    public class Functions
    {
        private IConfiguration _config;
        private string _usuario;
        private string _password;
        private ILogger logger;
        private Dictionary<string, HttpMethod> sitios;

        public Functions(IConfiguration config, ILogger<Functions> _logger)
        {
            _config = config;
            _usuario = _config["Usuario"];
            _password = _config["Password"];
            logger = _logger;

            sitios = new Dictionary<string, HttpMethod>
            {
                { "Projects/SyncProjects", HttpMethod.Post },
                { "DevopsProcess/GetBuildsDevOpsProccess", HttpMethod.Post },
                { "DevopsProcess/GetDeploymentsDevopsProccess", HttpMethod.Post },
                { "Sonar", HttpMethod.Get },
                { "Sonar/GetBuildSonar", HttpMethod.Get }
            };
        }

        //0 30 20 * * *
        //[TimerTrigger("0 0 21 * * *", RunOnStartup = true)] TimerInfo info,        ILogger logger
        [NoAutomaticTrigger]
        public void Run()
        {
            logger.LogInformation("Inicio procesamiento");
            var dic = new Dictionary<string, string>();
            foreach (var item in sitios)
            {
                var respuesta = HttpRequest(
                    item.Value,
                    $"{_config["UrlBase"]}{item.Key}",
                    logger);

                dic.Add(item.Key, respuesta.StatusCode.ToString());
                logger.LogInformation($"{_config["UrlBase"]}{item.Key}");
            }

            SendMessage(dic);

            logger.LogInformation("Fin procesamiento");
        }

        private void SendMessage(Dictionary<string, string> dic)
        {
            var apiKey = _config["Msg:KeysendGrid"];
            var client = new SendGridClient(apiKey);
            
            var from = new EmailAddress(_config["Msg:From"], _config["Msg:FromDescripcion"]);
            var list = new List<string>();
            _config.GetSection("Msg:To").Bind(list);

            List<EmailAddress> tos = new List<EmailAddress>();
            list.ForEach((i) => tos.Add(new EmailAddress(i)));

            var subject = _config.GetValue<string>("Msg:Subject").Replace('+',(char)243);
            var htmlContent = $"<strong>{_config["Msg:Titulo"]}</strong><br>";
            foreach (var reg in dic)
            {
                htmlContent += $"<p>» <strong>Api:</strong> {reg.Key} <strong>Resultado:</strong> {reg.Value}</p>";
            }
            htmlContent += "<p><img src='https://dddddd.blob.core.windows.net/images/logoXM.png' /></p>";
            htmlContent += "";

            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, "", htmlContent, false);
            msg.AddHeader("content", "text/html");
            msg.AddHeader("charset", "UTF-8");
            var response = client.SendEmailAsync(msg).Result;
        }

        private HttpResponseMessage HttpRequest(
            HttpMethod method,
            string uri,
            ILogger logger,
            int intento = 1)
        {
            HttpRequestMessage request;
            HttpResponseMessage response = new HttpResponseMessage();
            var cadena = uri.ToString();
            request = new HttpRequestMessage(method, cadena);
            //request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", Usuario, Password))));
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(_config.GetValue<int>("TimeOut"));
                try
                {
                    response = client.SendAsync(request).Result;
                    response = Reproceso(method, uri, logger, intento, response);
                }
                catch (Exception ex)
                {
                    //enviar un mensaje a devops
                    logger.LogInformation(ex.Message);
                    response = Reproceso(method, uri, logger, intento, response);
                }
            }

            return response;
        }

        private HttpResponseMessage Reproceso(
            HttpMethod method,
            string uri,
            ILogger logger,
            int intento,
            HttpResponseMessage response)
        {

            if (!response.IsSuccessStatusCode && intento < _config.GetValue<int>("Reintentos"))
            {
                return HttpRequest(method, uri, logger, intento + 1);
            }
            else
            {
                return response;
            }
        }
    }
}

//using ControlBoard.Api.Controllers;
//using ControlBoard.Domain.Commons;
//using ControlBoard.Domain.ValidationDeployments;
//using ControlBoard.Models;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Configuration;
//using Moq;
//using System;
//using System.Threading.Tasks;
//using Xunit;

//public class DevOpsProcessTests
//{

//    [Fact]
//    public async Task ValidationFileDeploy_WithEmptyFile_ReturnsBadRequest()
//    {
//        // Arrange
//        var httpUtilsMock = new Mock<IHttpUtils>();
//        var configMock = new Mock<IConfiguration>();
//        var deploymentsRepositoryMock = new Mock<IValidationsDeployments>();

//        var deploymentDomain = new DeploymentDomain(httpUtilsMock.Object, configMock.Object, deploymentsRepositoryMock.Object);

//        var controller = new DeploymentValidation(httpUtilsMock.Object, configMock.Object, deploymentsRepositoryMock.Object);

//        var emptyFile = new FileValidationModel
//        {
//            FUDDV_File = string.Empty
//        };

//        // Act
//        var result = await controller.ValidationFileDeploy(emptyFile) as ActionResult<string>;

//        // Assert
//        Assert.NotNull(result);
//        Assert.IsType<ActionResult<string>>(result);

//    }
//}

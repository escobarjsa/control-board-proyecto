using ControlBoard.Api.Controllers;
using ControlBoard.Domain.Builds;
using ControlBoard.Domain.Commons;
using ControlBoard.Domain.ValidationDeployments;
using ControlBoard.Domain.Deployments;  
using ControlBoard.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

public class DevopsProcessControllerTests
{
    private DevopsProcessController _controller;

    public DevopsProcessControllerTests()
    {
        var deploymentsMock = new Mock<ControlBoard.Domain.Deployments.IDeployments>();
        var buildsMock = new Mock<IBuilds>();
        var configMock = new Mock<IConfiguration>();
        var httpUtilsMock = new Mock<IHttpUtils>();

        _controller = new DevopsProcessController(httpUtilsMock.Object, deploymentsMock.Object, buildsMock.Object, configMock.Object);
    }

    [Fact]
    public async void GetDeploymentsDevops_ReturnsOkResult_WhenResultIsValid()
    {
        // Arrange

        // Act
        var result = await _controller.GetDeploymentsDevops();

        // Assert
        Assert.IsType<ActionResult<string>>(result);
    }

    [Fact]
    public async void GetBuildsDevOpsProccess_ReturnsOkResult_WhenResultIsValid()
    {
        // Arrange

        // Act
        var result = await _controller.GetBuildsDevOpsProccess();

        // Assert
        Assert.IsType<ActionResult<string>>(result);
    }
}

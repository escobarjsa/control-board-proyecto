using ControlBoard.Domain.Commons;
using ControlBoard.Api.Controllers;
using ControlBoard.Domain.Projects;
using ControlBoard.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using System.Threading.Tasks;
using Xunit;
using ControlBoard.Domain.ValidationDeployments;
using System.ComponentModel.DataAnnotations;

public class ProjectsControllerTests
{
    private ProjectsController _controller;

    public ProjectsControllerTests()
    {
        var projectsMock = new Mock<IProjects>();
        var configMock = new Mock<IConfiguration>();
        var httpUtilsMock = new Mock<IHttpUtils>();

        _controller = new ProjectsController(projectsMock.Object, configMock.Object, httpUtilsMock.Object);
    }

    [Fact]
    public void SyncProjectsToDataBase_ReturnsOkResult_WhenResultIsValid()
    {
        // Arrange

        // Act
        var result = _controller.SyncProjectsToDataBase().Result;

        // Assert
        Assert.IsType<ActionResult<string>>(result);

    }
}


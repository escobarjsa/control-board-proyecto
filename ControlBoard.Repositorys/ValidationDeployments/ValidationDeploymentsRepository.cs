﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ControlBoard.Domain.ValidationDeployments;
using ControlBoard.Models;

namespace ControlBoard.Repositorys.ValidationDeployments
{
    public class ValidationDeploymentsRepository : IValidationsDeployments
    {

        private HttpRequestMessage AddHeaderToken(ref HttpRequestMessage request, CredentialsModel config)
        {
            if (config.TokenRequired)
            {
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", config.Usser, config.Password))));
            }
            return request;
        }


        public async Task<Stream> FileDowload(CredentialsModel config, string UrlFile)
        {
            Stream estado = null;
            HttpRequestMessage request;
            var Url = String.Concat(config.UrlProject, "/_apis/tfvc/items?path=", UrlFile, "&download=true&includeContent=true");
            request = new HttpRequestMessage(HttpMethod.Get, Url);
            this.AddHeaderToken(ref request, config);
            using (var client = new HttpClient())
            {
                var response = client.SendAsync(request).Result;

                if (response.IsSuccessStatusCode)
                {
                    estado = await response.Content.ReadAsStreamAsync();
                }

            }
            return estado;
        }

        public async Task<List<string>> OnGetListItem(CredentialsModel config, string path)
        {
            List<string> enlace = new List<string>();
            var url = $"{config.UrlProject}/_apis/tfvc/items?scopepath={path}&download=false&includeContent=false&includeLinks=true&recursionLevel=full";
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", config.Usser, config.Password))));
            using (var client = new HttpClient())
            {
                var response = client.SendAsync(request).Result;
                if (response.IsSuccessStatusCode)
                {
                    var rta = await response.Content.ReadAsStringAsync();
                    JObject rss = JObject.Parse(rta);
                    for (var i = 0; i < ((JArray)rss["value"]).Count; i++)
                    {
                        enlace.Add(((JArray)rss["value"])[i]["path"].ToString());
                    }
                }
            }

            return enlace;
        }


        public async Task<bool> OnGet(CredentialsModel config, string file)
        {
            bool respuesta = false;
            var url = $"{config.UrlProject}/_apis/tfvc/items?scopePath={file}";

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", config.Usser, config.Password))));
            using (var client = new HttpClient())
            {
                var response = client.SendAsync(request).Result;
                string resultService = response.Content.ReadAsStringAsync().Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = JsonConvert.DeserializeObject<dynamic>(resultService);
                    if (result["value"].Count > 0)
                    {
                        respuesta = true;
                    }
                }
            }

            return respuesta;
        }

        public async Task<bool> OnGetAzure(CredentialsModel config, string urlFile)
        {
            bool respuesta = false;
            string ramaGit = string.Empty;

            if (urlFile.Contains("version=GB"))
            {
                ramaGit = urlFile.Split("version=GB")[1].Split("&")[0].Split("%")[0];
            }

            var pathfile = urlFile.Split("path=")[1].Split("&")[0];

            if (!urlFile.Contains("."))
            {
                if (urlFile.Contains("a=contents%"))
                {
                    var content = urlFile.Split("a=contents%")[1];

                    pathfile += "%" + content.Split("&")[0];
                }
                else if (urlFile.Contains("&version=T%"))
                {
                    var filetfvc = "/" + urlFile.Split("&version=T%")[1].Split("&")[0];

                    pathfile = pathfile.Replace("2F", "") + filetfvc;
                }
                else
                {
                    List<string> listComponent = urlFile.Split("%").ToList();

                    pathfile += "/" + listComponent[listComponent.Count - 1];
                }
            }

            List<string> listPaths = pathfile.Split("%").ToList();

            if (!urlFile.Contains("version=GB"))
            {
                listPaths.RemoveRange(0, 3);
            }

            string newPath = string.Empty;

            foreach (string item in listPaths)
            {
                newPath += "/" + item.Replace("2F", "");
            }

            string url = string.Empty;

            if (urlFile.Contains("version=GB"))
            {
                url = $"{config.UrlProject}/_apis/git/repositories/{config.ProjectName}/items?path={newPath}&version={ramaGit}&download=false&api-version=5.1";
            }
            else
            {
                url = $"{config.UrlProject}/_apis/tfvc/items?path={newPath}&download=false&api-version=5.1";
            }

            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(url);
            byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
            url = iso.GetString(isoBytes).Replace("???????", "").Replace("/20", "%20");


            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", config.Usser, config.Password))));
            using (var client = new HttpClient())
            {
                var response = client.SendAsync(request).Result;
                string resultService = response.Content.ReadAsStringAsync().Result;
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
            }

            return respuesta;
        }
    }
}

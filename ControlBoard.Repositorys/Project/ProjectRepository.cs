﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ControlBoard.Domain.Projects;
using ControlBoard.Models;
using ControlBoard.Repositorys.Commons;

namespace ControlBoard.Repositorys
{
    public class ProjectRepository : IProjects
    {
        private readonly ContextRepository _context;
        public ProjectRepository(ContextRepository context)
        {
            _context = context;
        }

        public List<ProjectEntity> GetProjects()
        {
            List<ProjectEntity> dataProjects = _context.Project.ToList();
            return dataProjects;
        }

        public void InsertProject(ProjectEntity Project)
        {
            _context.Project.Add(Project);
            _context.SaveChangesAsync().Wait();
        }

        public void SyncProjectsToDataBase()
        {
            throw new NotImplementedException();
        }
    }
}

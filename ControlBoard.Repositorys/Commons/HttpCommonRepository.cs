﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using ControlBoard.Domain.Commons;
using ControlBoard.Models;

namespace ControlBoard.Repositorys.Utils
{
    public class HttpCommonRepository : IHttpUtils
    {
   
        private HttpRequestMessage AddHeaderToken(ref HttpRequestMessage request, CredentialsModel config)
        {      
            if (config.TokenRequired) {
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", config.Usser, config.Password))));
            }
            return request;
        }

        public async Task<dynamic> Get(CredentialsModel config, string parameters)
        {
            dynamic result = null;
            HttpRequestMessage request;
            request = new HttpRequestMessage(HttpMethod.Get, config.UrlBase);
            this.AddHeaderToken(ref request, config);
            using (var client = new HttpClient())
            {
                var response = client.SendAsync(request).Result;
                string resultService = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<dynamic>(resultService);
                }
            }
            return result;
        }

        public System.Threading.Tasks.Task<dynamic> Post(string Url, ConfigModel config, string Params)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetProjectUrl(CredentialsModel config, string Params)
        {
            throw new NotImplementedException();
        }

        public async Task<Stream> FileDowload(CredentialsModel config, string UrlFile)
        {
            Stream estado = null;
            HttpRequestMessage request;
            var Url = String.Concat(config.UrlProject, "/_apis/tfvc/items?path=", UrlFile, "&download=true&includeContent=true");
            request = new HttpRequestMessage(HttpMethod.Get, Url);
            this.AddHeaderToken(ref request, config);
            using (var client = new HttpClient())
            {
                var response =  client.SendAsync(request).Result;

                if (response.IsSuccessStatusCode)
                {
                    estado = await response.Content.ReadAsStreamAsync();
                }

            }
            return estado;
        }

       
    }
}

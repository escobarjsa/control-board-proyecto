﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ControlBoard.Models;

namespace ControlBoard.Repositorys.Commons
{
    public class ContextRepository : DbContext
    {
        public ContextRepository(DbContextOptions<ContextRepository> options)
            : base(options)
        {
        }

        public DbSet<ProjectEntity> Project { get; set; }
        public DbSet<ICEntity> IntegracionContinua { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ICEntity>(entity =>
            {
                entity.Property(e => e.Inicio).HasDefaultValueSql("(getdate())");
            });
        }
    }
}

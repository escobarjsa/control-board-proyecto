﻿using ControlBoard.Domain.Deployments;
using ControlBoard.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Threading.Tasks;

namespace ControlBoard.Repositorys.Deployments
{
    public class DeploymentsRepository : IDeployments
    {

        private readonly IConfiguration _Config;
        public DeploymentsRepository(IConfiguration config)
        {
            _Config = config;
        }

        private HttpRequestMessage AddHeaderToken(ref HttpRequestMessage request, CredentialsModel config)
        {
            if (config.TokenRequired)
            {
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", config.Usser, config.Password))));
            }
            return request;
        }

        public async Task<dynamic> GetProjects(CredentialsModel config, object p)
        {
            dynamic result = null;
            HttpRequestMessage request;
            request = new HttpRequestMessage(HttpMethod.Get, config.UrlBase);
            this.AddHeaderToken(ref request, config);
            using (var client = new HttpClient())
            {
                var response = client.SendAsync(request).Result;
                string resultService = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<dynamic>(resultService);
                }
            }
            return result;
        }

        public string GetInitDatDataBase(string id)
        {
            var str = _Config["Settings:ConnectionString"];
            string fechaincial = String.Empty;
            using (SqlConnection conn = new SqlConnection(str))
            {
                conn.Open();
                var text = "";
                text = "select max(inicio) as inicio from dbo.despliegue_continuo_Temp where guid_proyecto='" + id + "'";

                using (SqlCommand cmd = new SqlCommand(text, conn))
                {
                    using (var dataReader = cmd.ExecuteReaderAsync())
                    {
                        if (dataReader.Result.Read())
                        {
                            if (dataReader.Result["inicio"].ToString() != "")
                                fechaincial = string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", Convert.ToDateTime(dataReader.Result["inicio"]).AddMilliseconds(100));
                        }
                    }
                }
            }

            return fechaincial;
        }

        public bool InsertDeployments(dynamic item, string IdProject)
        {
            var str = _Config["Settings:ConnectionString"];
            using (SqlConnection conn = new SqlConnection(str))
            {
                conn.Open();
                var dtserial = JsonConvert.DeserializeObject<EntidadCD>(item.ToString());

                DateTime inicio;
                var estado1 = DateTime.TryParse(dtserial.startedOn, out inicio);
                var fechaInicio = (!estado1 || dtserial.startedOn == "0001-01-01T00:00:00") ? dtserial.queuedOn : dtserial.startedOn;

                var estado2 = DateTime.TryParse(dtserial.completedOn, out inicio);
                var fechaFin = (!estado2 || dtserial.completedOn == "0001-01-01T00:00:00") ? dtserial.queuedOn : dtserial.completedOn;


                var estado3 = DateTime.TryParse(dtserial.queuedOn, out inicio);
                using (SqlCommand cmd = new SqlCommand("sp_insert_deploys", conn))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@guid_proyecto", SqlDbType.VarChar).Value = IdProject;
                    cmd.Parameters.Add("@id_cd", SqlDbType.VarChar).Value = dtserial.id;
                    cmd.Parameters.Add("@inicio", SqlDbType.DateTime).Value = fechaInicio;
                    cmd.Parameters.Add("@fin", SqlDbType.DateTime).Value = fechaFin;
                    cmd.Parameters.Add("@pipeline", SqlDbType.VarChar).Value = dtserial.releaseDefinition.name;
                    cmd.Parameters.Add("@ambiente", SqlDbType.VarChar).Value = dtserial.releaseEnvironment.name;
                    cmd.Parameters.Add("@estado", SqlDbType.VarChar).Value = dtserial.deploymentStatus;
                    cmd.Parameters.Add("@encolado", SqlDbType.DateTime).Value = dtserial.queuedOn;
                    cmd.Parameters.Add("@release_name", SqlDbType.VarChar).Value = dtserial.release.name;

                    var rows = cmd.ExecuteNonQuery();
                    return (rows == 1) ? true : false;

                }
            }
        }

        public bool InsertDeploymentsTemporalTable(dynamic item, string IdProject)
        {
            var str = _Config["Settings:ConnectionString"];
            using (SqlConnection conn = new SqlConnection(str))
            {
                conn.Open();
                var dtserial = JsonConvert.DeserializeObject<EntidadCD>(item.ToString());

                DateTime inicio;
                var estado1 = DateTime.TryParse(dtserial.startedOn, out inicio);
                var fechaInicio = (!estado1 || dtserial.startedOn == "0001-01-01T00:00:00") ? dtserial.queuedOn : dtserial.startedOn;

                var estado2 = DateTime.TryParse(dtserial.completedOn, out inicio);
                var fechaFin = (!estado2 || dtserial.completedOn == "0001-01-01T00:00:00") ? dtserial.queuedOn : dtserial.completedOn;


                var estado3 = DateTime.TryParse(dtserial.queuedOn, out inicio);
                using (SqlCommand cmd = new SqlCommand("sp_insert_deploys_temp", conn))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@guid_proyecto", SqlDbType.VarChar).Value = IdProject;
                    cmd.Parameters.Add("@id_cd", SqlDbType.VarChar).Value = dtserial.id;
                    cmd.Parameters.Add("@inicio", SqlDbType.DateTime).Value = fechaInicio;
                    cmd.Parameters.Add("@fin", SqlDbType.DateTime).Value = fechaFin;
                    cmd.Parameters.Add("@pipeline", SqlDbType.VarChar).Value = dtserial.releaseDefinition.name;
                    cmd.Parameters.Add("@pipeline_build", SqlDbType.VarChar).Value = ExtractionBuildPipeline(dtserial.release);
                    cmd.Parameters.Add("@ambiente", SqlDbType.VarChar).Value = dtserial.releaseEnvironment.name;
                    cmd.Parameters.Add("@estado", SqlDbType.VarChar).Value = dtserial.deploymentStatus;
                    cmd.Parameters.Add("@encolado", SqlDbType.DateTime).Value = dtserial.queuedOn;
                    cmd.Parameters.Add("@release_name", SqlDbType.VarChar).Value = dtserial.release.name;

                    var rows = cmd.ExecuteNonQuery();
                    return (rows == 1) ? true : false;

                }
            }
        }

        private static string ExtractionBuildPipeline(dynamic reslease)
        {

            string pipeline_name = String.Empty;
            foreach (var item in reslease.artifacts)
            {
                if (item.definitionReference != null && item.definitionReference.definition != null)
                {
                    pipeline_name = Convert.ToString(item.definitionReference.definition.name);
                    return pipeline_name;
                }
            }
            return pipeline_name;
        }
    }
}

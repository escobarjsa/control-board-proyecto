﻿using ControlBoard.Domain.Builds;
using ControlBoard.Models;
using ControlBoard.Repositorys.Commons;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Office2010.Word;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Build.Evaluation;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace ControlBoard.Repositorys.Builds
{

    public class BuildsRepository : IBuilds
    {
        private readonly ContextRepository _context; //instancia de ContextRepository para interactuar con la base de datos.

        private readonly IConfiguration _Config; // instancia de IConfiguration para acceder a la configuración de la aplicación.
        public BuildsRepository(IConfiguration config, ContextRepository context) //Constructor de la clase que inicializa las propiedades _Config y _context.
        {
            _Config = config;
            _context = context;
        }

        //Agrega un encabezado de autorización a la solicitud HTTP si se requiere un token.
        //Parámetros:
        //HttpRequestMessage request: La solicitud HTTP a la que se agregará el encabezado.
        //CredentialsModel config: Contiene la configuración necesaria para determinar si se requiere un token y sus credenciales.
        private HttpRequestMessage AddHeaderToken(ref HttpRequestMessage request, CredentialsModel config)
        {
            if (config.TokenRequired)
            {
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", config.Usser, config.Password))));
            }
            return request;
        }

        //Realiza una solicitud HTTP GET para obtener proyectos desde una URL especificada en config.
        //Usa AddHeaderToken para agregar el encabezado de autenticación.
        //Retorna un objeto dinámico que representa la respuesta deserializada del servicio.
        public async Task<dynamic> GetProjects(CredentialsModel config, object p)
        {
            dynamic result = null;
            HttpRequestMessage request;
            request = new HttpRequestMessage(HttpMethod.Get, config.UrlBase);
            this.AddHeaderToken(ref request, config);
            using (var client = new HttpClient())
            {
                var response = client.SendAsync(request).Result;
                string resultService = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<dynamic>(resultService);
                }
            }
            return result;
        }

        //Consulta la base de datos para obtener la fecha de inicio de un proyecto usando un procedimiento almacenado.
        //Retorna una cadena que representa la fecha inicial.
        public string GetInitDatDataBase(string id)
        {
            var str = _Config["Settings:ConnectionString"];
            string fechaincial = String.Empty;
            using (SqlConnection conn = new SqlConnection(str))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("sp_get_datetime_last_build", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
                    var dataResult = cmd.ExecuteReaderAsync();
                    if (dataResult.Result.Read())
                    {
                        if (dataResult.Result["inicio"].ToString() != "")
                            fechaincial = string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", Convert.ToDateTime(dataResult.Result["inicio"]).AddMilliseconds(100));
                    }
                }
            }
            return fechaincial;
        }

        //Inserta la información de una construcción en la base de datos.
        //Deserializa la información de la construcción, convierte las fechas y ejecuta un procedimiento almacenado para insertar los datos.
        //Retorna true si la inserción fue exitosa.
        //public bool InsertBuilds(dynamic item, string IdProject)
        //{
        //    var str = _Config["Settings:ConnectionString"];
        //    using (SqlConnection conn = new SqlConnection(str))
        //    {
        //        conn.Open();
        //        DateTime inicio;
        //        var dtserial = JsonConvert.DeserializeObject<EntidadCI>(item.ToString());
        //        var estado1 = DateTime.TryParse(dtserial.startTime, out inicio);
        //        var fechaInicio = (!estado1 || dtserial.startTime == "0001-01-01T00:00:00") ? string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", Convert.ToDateTime(dtserial.queueTime)) : string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", Convert.ToDateTime(dtserial.startTime));

        //        var estado2 = DateTime.TryParse(dtserial.finishTime, out inicio);
        //        var fechaFinal = (!estado2 || dtserial.finishTime == "0001-01-01T00:00:00") ? string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", Convert.ToDateTime(dtserial.queueTime)) : string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", Convert.ToDateTime(dtserial.finishTime));

        //        var queueTime = string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", Convert.ToDateTime(dtserial.queueTime));

        //        using (SqlCommand cmd = new SqlCommand("sp_insert_builds", conn))
        //        {

        //            cmd.CommandType = CommandType.StoredProcedure;

        //            cmd.Parameters.Add("@guid_proyecto", SqlDbType.NVarChar).Value = IdProject;
        //            cmd.Parameters.Add("@id_ci", SqlDbType.NVarChar).Value = dtserial.id;
        //            cmd.Parameters.Add("@inicio", SqlDbType.DateTime).Value = fechaInicio;
        //            cmd.Parameters.Add("@fin", SqlDbType.DateTime).Value = fechaFinal;
        //            cmd.Parameters.Add("@pipeline", SqlDbType.NVarChar).Value = dtserial.definition.name;
        //            cmd.Parameters.Add("@build_number", SqlDbType.NVarChar).Value = dtserial.buildNumber;
        //            cmd.Parameters.Add("@estado", SqlDbType.NVarChar).Value = dtserial.result;
        //            cmd.Parameters.Add("@encolado", SqlDbType.DateTime).Value = queueTime;

        //            var rows = cmd.ExecuteNonQuery();
        //            return (rows == 1) ? true : false;
        //        }
        //    }
        //}

        // Define el método InsertBuilds que inserta datos de builds en la base de datos.
        // Recibe un objeto dinámico 'item' que contiene los datos del build y un string 'IdProject' que representa el ID del proyecto.
        public bool InsertBuilds(dynamic item, string IdProject)
        {
            // Obtiene la cadena de conexión de la configuración de la aplicación.
            var str = _Config["Settings:ConnectionString"];

            // Inicia un bloque using para garantizar que la conexión SQL se cierre automáticamente al finalizar el bloque.
            using (SqlConnection conn = new SqlConnection(str))
            {
                // Abre la conexión a la base de datos.
                conn.Open();

                // Declara una variable para almacenar la fecha de inicio parseada.
                DateTime inicio;

                // Deserializa el objeto 'item' a un tipo específico 'EntidadCI' para acceder a sus propiedades de manera segura.
                var dtserial = JsonConvert.DeserializeObject<EntidadCI>(item.ToString());

                // Intenta parsear la fecha de inicio. Si falla, retorna falso.
                if (!DateTime.TryParse(dtserial.startTime, out inicio))
                {
                    return false;
                }

                // Comprueba si la fecha de inicio es futura respecto a la fecha actual y, en ese caso, retorna falso.
                if (inicio > DateTime.Now)
                {
                    return false;
                }

                // Convierte la fecha de inicio a un formato específico de string.
                var fechaInicio = inicio.ToString("yyyy-MM-ddTHH:mm:ss.FFF");

                // Declara una variable para almacenar la fecha de finalización parseada.
                DateTime fin;

                // Intenta parsear la fecha de finalización y usa la fecha de inicio como fallback si falla.
                var estado2 = DateTime.TryParse(dtserial.finishTime, out fin);
                var fechaFinal = estado2 ? fin.ToString("yyyy-MM-ddTHH:mm:ss.FFF") : fechaInicio;

                // Declara explícitamente la variable qTime para usarla en el método TryParse.
                DateTime qTime;

                // Intenta parsear la hora de encolado y usa la fecha de inicio como fallback si falla.
                var queueTime = DateTime.TryParse(dtserial.queueTime, out qTime)
                                ? qTime.ToString("yyyy-MM-ddTHH:mm:ss.FFF")
                                : fechaInicio;

                // Inicia otro bloque using para el comando SQL asegurando que los recursos se liberen adecuadamente.
                using (SqlCommand cmd = new SqlCommand("sp_insert_builds", conn))
                {
                    // Especifica que el tipo de comando es un procedimiento almacenado.
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Agrega los parámetros requeridos por el procedimiento almacenado, asignando los valores correspondientes.
                    cmd.Parameters.Add("@guid_proyecto", SqlDbType.NVarChar).Value = IdProject;
                    cmd.Parameters.Add("@id_ci", SqlDbType.NVarChar).Value = dtserial.id;
                    cmd.Parameters.Add("@inicio", SqlDbType.DateTime).Value = fechaInicio;
                    cmd.Parameters.Add("@fin", SqlDbType.DateTime).Value = fechaFinal;
                    cmd.Parameters.Add("@pipeline", SqlDbType.NVarChar).Value = dtserial.definition.name;
                    cmd.Parameters.Add("@build_number", SqlDbType.NVarChar).Value = dtserial.buildNumber;
                    cmd.Parameters.Add("@estado", SqlDbType.NVarChar).Value = dtserial.result;
                    cmd.Parameters.Add("@encolado", SqlDbType.DateTime).Value = queueTime;

                    // Ejecuta el comando SQL y obtiene el número de filas afectadas.
                    var rows = cmd.ExecuteNonQuery();

                    // Retorna verdadero si se afectó exactamente una fila, lo que indica una inserción exitosa.
                    return (rows == 1);
                }
            }
        }

        //public bool InsertBuilds(dynamic item, string IdProject)
        //{
        //    var str = _Config["Settings:ConnectionString"];

        //    using (SqlConnection conn = new SqlConnection(str))
        //    {
        //        conn.Open();
        //        DateTime inicio;
        //        var dtserial = JsonConvert.DeserializeObject<EntidadCI>(item.ToString());

        //        if (!DateTime.TryParse(dtserial.startTime, out inicio))
        //        {
        //            return false;
        //        }

        //        // Verifica si la fecha de inicio es futura respecto a la fecha actual
        //        if (inicio > DateTime.Now)
        //        {
        //            return false;
        //        }

        //        // Verifica si la fecha de inicio es posterior al año 2025
        //        if (inicio > new DateTime(2025, 12, 31))
        //        {
        //            return false; // No procesa pipelines después de 2025
        //        }

        //        var fechaInicio = inicio.ToString("yyyy-MM-ddTHH:mm:ss.FFF");
        //        DateTime fin;
        //        var estado2 = DateTime.TryParse(dtserial.finishTime, out fin);
        //        var fechaFinal = estado2 ? fin.ToString("yyyy-MM-ddTHH:mm:ss.FFF") : fechaInicio;
        //        DateTime qTime;
        //        var queueTime = DateTime.TryParse(dtserial.queueTime, out qTime)
        //                        ? qTime.ToString("yyyy-MM-ddTHH:mm:ss.FFF")
        //                        : fechaInicio;

        //        using (SqlCommand cmd = new SqlCommand("sp_insert_builds", conn))
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            cmd.Parameters.Add("@guid_proyecto", SqlDbType.NVarChar).Value = IdProject;
        //            cmd.Parameters.Add("@id_ci", SqlDbType.NVarChar).Value = dtserial.id;
        //            cmd.Parameters.Add("@inicio", SqlDbType.DateTime).Value = fechaInicio;
        //            cmd.Parameters.Add("@fin", SqlDbType.DateTime).Value = fechaFinal;
        //            cmd.Parameters.Add("@pipeline", SqlDbType.NVarChar).Value = dtserial.definition.name;
        //            cmd.Parameters.Add("@build_number", SqlDbType.NVarChar).Value = dtserial.buildNumber;
        //            cmd.Parameters.Add("@estado", SqlDbType.NVarChar).Value = dtserial.result;
        //            cmd.Parameters.Add("@encolado", SqlDbType.DateTime).Value = queueTime;

        //            var rows = cmd.ExecuteNonQuery();
        //            return (rows == 1);
        //        }
        //    }
        //}


        //Obtiene el GUID de un proyecto específico buscándolo por nombre en la base de datos.
        //Retorna el GUID como una cadena.
        public string GetProjectDevOps(string Project)
        {
            ProjectEntity dataProjects = _context.Project.Where(x => x.Nombre == Project).FirstOrDefault();

            return dataProjects.Guid;
        }

        //Verifica si un proyecto con el GUID especificado existe en la base de datos.
        //Retorna true si el proyecto existe.
        public bool FindProjectDevOps(string GuidProject)
        {
            ICEntity dataProjects = _context.IntegracionContinua.Where(x => x.Guid == GuidProject).FirstOrDefault();

            if (dataProjects == null)
            {
                return false;
            }

            return true;
        }
    }
}

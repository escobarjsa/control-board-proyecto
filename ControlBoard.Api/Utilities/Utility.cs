﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ControlBoard.Api.Utilities
{
    public class Utility
    {
        public static IConfiguration Configuration { get; set; }

        public static string GetAppSetting(string Grupo, string Key)
        {
            if (Configuration == null)
            {
                var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
                Configuration = builder.Build();
            }
            return Configuration.GetSection(Grupo)[Key].ToString();
        }
    }
}

﻿using ControlBoard.Api.Model;
using ControlBoard.Domain.Builds;
using ControlBoard.Domain.Commons;
using ControlBoard.Domain.Deployments;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace ControlBoard.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevopsProcessController : ControllerBase
    {

        private readonly DeploymentsDomain _DeploymentsDomain;
        private readonly BuildsDomain _BuildsDomain;
        public DevopsProcessController(IHttpUtils httpUtils, IDeployments deployments, IBuilds builds, IConfiguration config)
        {
            _DeploymentsDomain = new DeploymentsDomain(httpUtils, deployments, config);
            _BuildsDomain = new BuildsDomain(httpUtils, builds, config);
        }


        //[HttpPost]
        //[Authorize]
        [Route("GetDeploymentsDevopsProccess")]
        public async Task<ActionResult<string>> GetDeploymentsDevops()
        {
            try
            {
                var result = _DeploymentsDomain.InsertDeploymentsDevOpsProccess();
                return Ok(result);
            }
            catch (Exception ex)
            {

                return BadRequest(ex);
            }
        }


        //[HttpPost]
        // [Authorize]
        [Route("GetBuildsDevOpsProccess")]
        public async Task<ActionResult<string>> GetBuildsDevOpsProccess()
        {
            try
            {
                var result = await _BuildsDomain.InsertBuildDevOpsProccess();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        //[HttpPost]
        // [Authorize]
        [Route("GetProjectDevOpsProccess")]
        //public ActionResult<string> GetProjectDevOpsProccess(Project project)
        public ActionResult<string> GetProjectDevOpsProccess(Project project)
        {
            try
            {
                string result = _BuildsDomain.GetProjectDevOpsProccess(project.ProjectName);
                return Ok(result);
               
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


    }
}

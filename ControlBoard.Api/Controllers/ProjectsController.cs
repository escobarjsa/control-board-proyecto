﻿using ControlBoard.Domain.Commons;
using ControlBoard.Domain.Projects;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace ControlBoard.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        public readonly ProjectDomain _projectDomain;
        public ProjectsController(
            IProjects projects, 
            IConfiguration config,
            IHttpUtils httpUtils
            )
        {
            _projectDomain = new ProjectDomain(projects, config, httpUtils);
        }

        [HttpPost]
       // [Authorize]
        [Route("SyncProjects")]
        public async Task<ActionResult<string>> SyncProjectsToDataBase() {
            var result =  _projectDomain.SyncProjectsToDataBase();
            if (result.IsValid) {
                return Ok(result);
            }
            return BadRequest();
        }
    }
}

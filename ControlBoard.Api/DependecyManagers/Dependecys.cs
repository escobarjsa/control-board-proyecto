﻿using Microsoft.Extensions.DependencyInjection;
using ControlBoard.Domain.Builds;
using ControlBoard.Domain.Commons;
using ControlBoard.Domain.Deployments;
using ControlBoard.Domain.Projects;
using ControlBoard.Domain.ValidationDeployments;
using ControlBoard.Repositorys;
using ControlBoard.Repositorys.Builds;
using ControlBoard.Repositorys.Deployments;
using ControlBoard.Repositorys.Utils;
using ControlBoard.Repositorys.ValidationDeployments;

namespace ControlBoard.Api.DependecyManagers
{
    public class Dependecys
    {
        public void ConfigureServices(ref IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddScoped<IHttpUtils, HttpCommonRepository>();
            services.AddScoped<IProjects, ProjectRepository>();
            services.AddScoped<IValidationsDeployments, ValidationDeploymentsRepository>();
            services.AddScoped<Domain.Deployments.IDeployments, DeploymentsRepository>();
            services.AddScoped<IBuilds, BuildsRepository>();
            services.AddControllers().AddNewtonsoftJson(x =>
             x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }
    }
}

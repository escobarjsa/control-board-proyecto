﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ControlBoard.Models;

namespace ControlBoard.Domain.Builds
{
    public interface IBuilds
    {
        Task<dynamic> GetProjects(CredentialsModel credentials, object p);
        string GetInitDatDataBase(string id);
        bool InsertBuilds(dynamic item, string Idproject);
        string GetProjectDevOps(string project);
        bool FindProjectDevOps(string guidProjects);
    }
}

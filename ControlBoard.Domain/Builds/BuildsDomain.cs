﻿using ControlBoard.Domain.Commons;
using ControlBoard.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ControlBoard.Domain.Builds
{
    public class BuildsDomain
    {
        private readonly IHttpUtils _HttpUtils;
        private readonly IBuilds _BuildsRepository;
        private readonly IConfiguration _Config;
        public readonly CredentialsSettings _CredentialsSettings;
        private InsertDeploymentsResult result;

        public BuildsDomain(IHttpUtils httpUtils, IBuilds builds, IConfiguration config)
        {
            _HttpUtils = httpUtils;
            _Config = config;
            _BuildsRepository = builds;
            _CredentialsSettings = new CredentialsSettings(config);
            result = new InsertDeploymentsResult { ErrorMessage = new List<string>(), SuccessMessage = new List<string>() };
        }

        public async Task<InsertDeploymentsResult> InsertBuildDevOpsProccess()
        {
            //InitProccessInserBuildServicedo();
            InitProccessInserBuildAzure();
            return result;
        }

        public void InitProccessInserBuildServicedo()
        {

            CredentialsModel credentials = _CredentialsSettings.GetCredentialsServicedo();
            var collections = _Config["Settings:CollectionTFS"].Split(',');

            foreach (var item in collections)
            {
                credentials.Callection = item;
                GetProjectByCollection(credentials, item);
            }
        }

        public void InitProccessInserBuildAzure()
        {
            CredentialsModel credentials = _CredentialsSettings.GetCredentialsGeneralAzure();
            var collections = _Config["Settings:CollectionVSTS"].Split(',');

            foreach (var item in collections)
            {
                credentials.Callection = item;
                GetProjectByCollection(credentials, item);
            }
        }

        public async void GetProjectByCollection(CredentialsModel credentials, string collection)
        {
            credentials.UrlBase = credentials.UrlProject.Replace("{collection}", collection) + "?$top=500&api-version=5.0";
            var projects = await _BuildsRepository.GetProjects(credentials, null);

            foreach (var item in projects["value"])
            {
                try
                {
                    var Project = JsonConvert.DeserializeObject<ProjectDTO>(item.ToString());
                    var BuildsProject = await GetBuildsProject(Project, credentials);
                    InsertBuildsDataBase(Project, BuildsProject);
                }
                catch (Exception ex)
                {
                    result.ErrorMessage.Add("No se pudo insertar registro error : " + ex.Message);
                    continue;
                }
            }

        }

        public string GetProjectDevOpsProccess(string project)
        {
            if (project != null && !string.IsNullOrEmpty(project))
            {
                var projectName = project.Split("/");
                if (projectName.Length > 1)
                {
                    string guidProjects = _BuildsRepository.GetProjectDevOps(projectName[4]);
                    var result = _BuildsRepository.FindProjectDevOps(guidProjects);
                    if (result)
                    {
                        return "Tiene devops";
                    }
                    return "No tiene devops";
                }
                return "El string no tiene el formato correcto";
            }
            return "El string esta vacio";
        }

        public async Task<dynamic> GetBuildsProject(ProjectDTO project, CredentialsModel credentials)
        {

            string fechaInicial = _BuildsRepository.GetInitDatDataBase(project.Id);
            string cadena = "/_apis/build/builds?api-version=5.0&$top=100&queryOrder=queueTimeAscending";
            if (fechaInicial != "")
            {
                cadena = string.Format("{0}&minStartedTime={1}", cadena, fechaInicial);
            }
            credentials.UrlBase = $"{credentials.UrlBaseOriginal}/{credentials.Callection}/{project.name}{cadena}";
            var Builds = await _HttpUtils.Get(credentials, null);

            return Builds;
        }

        private void InsertBuildsDataBase(ProjectDTO project, dynamic BuildsProject)
        {
            List<object> listaCI = new List<object>();

            for (var i = 0; i < ((JArray)BuildsProject["value"]).Count; i++)
            {
                listaCI.Add(((JArray)BuildsProject["value"])[i]);
            }

            foreach (var item in listaCI)
            {
                bool res = _BuildsRepository.InsertBuilds(item, project.Id);
                if (!res)
                {
                    result.ErrorMessage.Add($"Resgisto existente y no actualizado,{project.name}");
                }
                result.SuccessMessage.Add($"Resgisto actualizado con exito,{project.name}");
            }
        }

        /*public ActionResult<string> GetResults()
        {
            
            project.ProjectName = "https://dev.azure.com/celuladevopsj/Celula/_git/Control-Board";

            if (project != null && !string.IsNullOrEmpty(project.ProjectName))
            {
                var projectName = project.ProjectName.Split("/");
                if (projectName.Length > 1)
                {
                    bool result = _BuildsDomain.GetProjectDevOpsProccess(projectName[4]);

                    if (result)
                    {
                        return new ObjectResult("Tiene devops") { StatusCode = 200 };
                    }
                    return new ObjectResult("No tiene devops") { StatusCode = 201 };
                }
                return new ObjectResult("El string no tiene el formato correcto") { StatusCode = 201 };
            }
            else
            {
                return new ObjectResult("El string esta vacio") { StatusCode = 201 };
            }
        }
        */
    }
}

﻿using ControlBoard.Domain.Commons;
using ControlBoard.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ControlBoard.Domain.Deployments
{
    public class DeploymentsDomain
    {
        private readonly IHttpUtils _HttpUtils;
        private readonly IDeployments _DeploymentsRepository;
        private readonly IConfiguration _Config;
        public readonly CredentialsSettings _CredentialsSettings;
        private InsertDeploymentsResult result;

        public DeploymentsDomain(IHttpUtils httpUtils, IDeployments deployments, IConfiguration config)
        {
            _HttpUtils = httpUtils;
            _Config = config;
            _DeploymentsRepository = deployments;
            _CredentialsSettings = new CredentialsSettings(config);
            result = new InsertDeploymentsResult { ErrorMessage = new List<string>(), SuccessMessage = new List<string>() };
        }


        public InsertDeploymentsResult InsertDeploymentsDevOpsProccess()
        {
            //InitProccessInserDeploymentServicedo();
            InitProccessInserDeploymentAzure();
            return result;
        }

        public void InitProccessInserDeploymentServicedo()
        {

            CredentialsModel credentials = _CredentialsSettings.GetCredentialsServicedo();
            var collections = _Config["Settings:CollectionTFS"].Split(',');

            foreach (var item in collections)
            {
                credentials.Callection = item;
                GetProjectByCollection(credentials, item);
            }
        }

        public void InitProccessInserDeploymentAzure()
        {
            CredentialsModel credentials = _CredentialsSettings.GetCredentialsGeneralAzure();
            var collections = _Config["Settings:CollectionVSTS"].Split(',');

            foreach (var item in collections)
            {
                credentials.Callection = item;
                GetProjectByCollection(credentials, item);
            }
        }


        public async void GetProjectByCollection(CredentialsModel credentials, string collection)
        {
            credentials.UrlBase = credentials.UrlProject.Replace("{collection}", collection) + "?$top=500&api-version=5.0";
            var projects = await _DeploymentsRepository.GetProjects(credentials, null);

            foreach (var item in projects["value"])
            {
                try
                {
                    ProjectDTO Project = JsonConvert.DeserializeObject<ProjectDTO>(item.ToString());
                    var deploymentsProject = await GetDeploymentsProject(Project, credentials);
                    InsertDeploymentsDataBase(Project, deploymentsProject);
                    InsertDeploymentsTemporalTable(Project, deploymentsProject);
                }
                catch (Exception ex)
                {
                    result.ErrorMessage.Add("No se pudo insertar registro error : " + ex.Message);
                    continue;
                }
            }

        }

        public async Task<dynamic> GetDeploymentsProject(ProjectDTO project, CredentialsModel credentials)
        {
            string fechaInicial = _DeploymentsRepository.GetInitDatDataBase(project.Id);
            string cadena = "/_apis/release/deployments?api-version=5.0&$top=100&queryOrder=ascending";
            if (fechaInicial != "")
            {
                cadena = string.Format("{0}&minStartedTime={1}", cadena, fechaInicial);
            }

            credentials.UrlBase = $"{credentials.UrlBaseOriginal}/{credentials.Callection}/{project.name}{cadena}";

            if (!string.IsNullOrEmpty(credentials.UrlBaseRelease))
            {
                credentials.UrlBase = $"{credentials.UrlBaseRelease}/{credentials.Callection}/{project.name}{cadena}";
            }

            
            var deployments = await _HttpUtils.Get(credentials, null);
            return deployments;

        }

        private void InsertDeploymentsDataBase(ProjectDTO project, dynamic deploymentsProject)
        {
            List<object> listaCD = new List<object>();

            for (var i = 0; i < ((JArray)deploymentsProject["value"]).Count; i++)
            {
                listaCD.Add(((JArray)deploymentsProject["value"])[i]);
            }

            foreach (var item in listaCD)
            {
                bool res = _DeploymentsRepository.InsertDeployments(item, project.Id);
                if (!res)
                {
                    result.ErrorMessage.Add($"Resgisto existente y no actualizado,{project.name}");
                }
                result.SuccessMessage.Add($"Resgisto actualizado con exito,{project.name}");
            }
        }

        private void InsertDeploymentsTemporalTable(ProjectDTO project, dynamic deploymentsProject)
        {
            List<object> listaCD = new List<object>();

            for (var i = 0; i < ((JArray)deploymentsProject["value"]).Count; i++)
            {
                listaCD.Add(((JArray)deploymentsProject["value"])[i]);
            }

            foreach (var item in listaCD)
            {
                bool res = _DeploymentsRepository.InsertDeploymentsTemporalTable(item, project.Id);
                if (!res)
                {
                    result.ErrorMessage.Add($"Resgisto existente y no actualizado en tabla temporal,{project.name}");
                }
                result.SuccessMessage.Add($"Resgisto actualizado con exito en tabla temporal,{project.name}");
            }
        }


    }
}

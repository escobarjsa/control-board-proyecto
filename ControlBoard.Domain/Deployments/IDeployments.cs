﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ControlBoard.Models;

namespace ControlBoard.Domain.Deployments
{
    public interface IDeployments
    {
        Task<dynamic> GetProjects(CredentialsModel credentials, object p);
        string GetInitDatDataBase(string id);
        bool InsertDeployments(dynamic item, string Idproject);
        bool InsertDeploymentsTemporalTable(dynamic item, string Idproject);
    }
}

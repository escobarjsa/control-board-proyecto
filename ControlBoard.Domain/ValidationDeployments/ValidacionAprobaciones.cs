﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ControlBoard.Models;

namespace ControlBoard.Domain.ValidationDeployments
{
    public class ValidacionAprobaciones
    {
        private readonly IConfiguration _Config;
        private readonly IValidationsDeployments _DeploymentsRepository;

        public string Colection { get; set; }
        private string NombreTarea { get; set; }

        public ValidacionAprobaciones(IConfiguration config, IValidationsDeployments IDeployments)
        {
            _Config = config;
            _DeploymentsRepository = IDeployments;
        }

        public bool ValidacionNombres(string path, RespuestaOperacion respuesta, CredentialsModel credentials)
        {
            bool estado = false;
            NombreTarea = string.Empty;
            var nombresValidos = _Config["Settings:NombresMsgValidos"].Split(",");
            var lista = _DeploymentsRepository.OnGetListItem(credentials, path).Result;

            foreach (var ii in nombresValidos)
            {
                var nombre = string.IsNullOrEmpty(NombreTarea) ? ii : (NombreTarea + " " + ii);
                var consulta = lista.Where(x => x.Contains(nombre) && x.Contains(".msg"));
                if (consulta.Count() > 0)
                {
                    using (var recurso = _DeploymentsRepository.FileDowload(credentials, consulta.First()).Result)
                    {
                        if (recurso.Length > 0)
                        {
                            try
                            {
                                using (var msg = new MsgReader.Outlook.Storage.Message(recurso, System.IO.FileAccess.Read))
                                {
                                    respuesta.Aprobador = msg.Sender.DisplayName;
                                }

                                estado = true;
                            }
                            catch
                            {
                                respuesta.Observaciones.Add("Error en la lectura de la aprobación del usuario.");
                                respuesta.Estado = 206;
                            }

                            break;
                        }
                    }
                }
            }

            return estado;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using ControlBoard.Models;

namespace ControlBoard.Domain.ValidationDeployments
{
    public interface IDeployments
    {
        Task<Stream> FileDowload(CredentialsModel config, string urlFile);
        Task<List<string>> OnGetListItem(CredentialsModel config, string urlFile);
        Task<bool> OnGet(CredentialsModel config, string urlFile);
        Task<bool> OnGetAzure(CredentialsModel config, string urlFile);
    }
}

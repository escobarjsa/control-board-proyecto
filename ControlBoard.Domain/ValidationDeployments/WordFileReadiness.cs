﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using ControlBoard.Domain.Commons;
using ControlBoard.Models;

namespace ControlBoard.Domain.ValidationDeployments
{
    public class WordFileReadiness
    {
        List<CadenasEncontradas> cadenas;
        public CredentialsModel Config { get; set; }
        public Entidades Entidad { get; set; }
        private readonly IValidationsDeployments _IDeployments;


        public WordFileReadiness(Entidades entidad, List<CadenasEncontradas> str, CredentialsModel credentials, IValidationsDeployments _deplymentsRepository)
        {
            Entidad = entidad;
            cadenas = str;
            Config = credentials;
            _IDeployments = _deplymentsRepository;
        }


        //public void LecturaWord(RespuestaOperacion respuesta, string filepath)
        public void LecturaWord(RespuestaOperacion respuesta, string filepath, Stream stream)
        {

            var nombre = String.Empty;

            if (filepath.Contains("%"))
            {
                nombre = filepath.Substring(filepath.LastIndexOf("%"));
            }
            else
            {
                nombre = filepath.Substring(filepath.LastIndexOf("/"));
            }


            foreach (var cadena in cadenas)
            {
                cadena.Texto = "";
                cadena.XMLformat = "";
            }

            using (WordprocessingDocument wordDocument =
                                WordprocessingDocument.Open(stream, false))
            {
                Body body = wordDocument.MainDocumentPart.Document.Body;

                for (var x = 0; x < cadenas.Count - 1; x++)
                {
                    for (var i = 0; i < body.ChildElements.Count; i++)
                    {
                        if (body.ChildElements.GetItem(i).InnerText.Trim().ToUpper().Equals(cadenas[x].Cadena.Trim().ToUpper()))
                        {
                            cadenas[x].Posicion = i;
                            cadenas[x].Coincidencias += 1;
                        }
                    }
                }

                //ultima posicion del documento
                cadenas[cadenas.Count - 1].Posicion = body.ChildElements.Count;
                cadenas[cadenas.Count - 1].Coincidencias = 1;
                ///obtener texto entre posiciones encontradas
                ///es sensible a los caracteres

                for (var j = 0; j < cadenas.Count - 1; j++)
                {
                    try
                    {
                        if (cadenas[j].Coincidencias >= 1)
                        {
                            for (var i = cadenas[j].Posicion + 1; i < cadenas[j + 1].Posicion; i++)
                            {
                                cadenas[j].Texto += " " + body.ChildElements.GetItem(i).InnerText;
                                cadenas[j].XMLformat += body.ChildElements.GetItem(i).InnerXml;
                            }

                            if ((string.IsNullOrEmpty(cadenas[j].Texto.Trim()) || cadenas[j].Texto.Trim() == "NA" || cadenas[j].Texto.Trim() == "N/A")
                                && cadenas[j].AplicaLectura == true)
                            {
                                respuesta.Observaciones.Add($"La sección '{cadenas[j].Cadena}' del documento {nombre} mapeada en el formato FUDDV como requerida no se encuentra diligenciada.");
                            }
                        }
                        else
                        {
                            respuesta.Observaciones.Add($"La sección '{cadenas[j].Cadena}' no se encuentra en el documento {nombre}.");
                        }
                    }
                    catch (Exception)
                    {
                        throw new Exception($"Error en la lectura del manual de instalación {nombre}.");
                    }
                }
            }

            //validacion secciones sin datos
            //con el fin de verificar si colocaron algo alli
            ValidacionDeSeccionesNodiligenciadas(respuesta);

            ///interpretar xml
            LeerRutasTabla(cadenas, respuesta);
            // LeerRutasfueraTabla();

            ///validar existencia enlaces relacionados entre secciones.
            /// si dentro de la seccion cuenta con un \\xxx o $xxxx/proyecto
            /// se procede con 
            /// obtener path
            /// hay rutas que refieren a ala carpeta de fuentes $/INDICA/DLLO/TASK1374126/FUENTES/Br1PRY5/SOLUCIONAPP/SETUP/SERVIDOR/ServicioReportes"
            /// si se quieren validar estas rutas se tendria que descargar toda la solucion, lo que se demoraria mucho.
            /// o referencian a $/INDICA/BASE/DOCUMENTACION/CONSTRUCCION/MANUALES
            LeerRutasfueraTabla(respuesta);
            //LeerRutasEnPlano(respuesta, nombre);
            IdentificacionTipoDireccionArchivo();


            /// - verificar que el archivo exista
            ValidarExistenciaArchivos(respuesta);
            //estado de la operacion
            respuesta.Estado = (respuesta.Observaciones.Count > 0) ? 206 : 200;
        }

        private void ValidarExistenciaArchivos(RespuestaOperacion respuesta)
        {
            foreach (var cadena in cadenas)
            {
                for (var i = 0; i < cadena.Enlace.Count; i++)
                {
                    //var rr = Archivo.TransformacionRutaInverso(Entidad, cadena.Enlace[i].Direccion);
                    var rutaarch = cadena.Enlace[i].Direccion;
                    bool result = false; 

                    if (rutaarch.Contains("https://dev.azure.com/"))
                    {
                        //var rutaarchAzure = rutaarch.Split("path=")[1];
                        //result = _IDeployments.OnGetAzure(Config, rutaarchAzure).Result;
                        result = _IDeployments.OnGetAzure(Config, rutaarch).Result;


                    }
                    else {
                        result = _IDeployments.OnGet(Config, rutaarch).Result; 
                    }

                    if (!result)
                    {
                        var tipo = cadena.Enlace[i].Archivo ? "El archivo" : "La ruta";
                        respuesta.Observaciones.Add($"{tipo} {rutaarch} de la sección '{cadena.Cadena}' no existe.");
                        Console.WriteLine($"{tipo} {rutaarch} de la seccion '{cadena.Cadena}' no existe.");
                    }

                }
            }
        }

      

        private void ValidacionDeSeccionesNodiligenciadas(RespuestaOperacion respuesta)
        {
            foreach (var item in cadenas)
            {
                if (!item.AplicaLectura && (item.Texto.Trim() != "NA" && item.Texto.Trim() != "" && item.Texto.Trim().Replace(" ", "") != "NANA" && item.Texto.Trim().Replace(" ", "") != "N/A"))
                {
                    respuesta.Estado = 206;
                    respuesta.Observaciones.Add($"En el documento de instalación hay actividades programadas para '{item.Cadena}'" +
                        "pero en el formato FUDDV este grupo está como no requerido para este despliegue en producción.");
                }
            }
        }

        private void IdentificacionTipoDireccionArchivo()
        {
            foreach (var cadena in cadenas)
            {
                for (var i = 0; i < cadena.Enlace.Count; i++)
                {
                    if (!string.IsNullOrEmpty(cadena.Enlace[i].Direccion))
                    {
                        cadena.Enlace[i].Archivo = StringUtils.ExpresionCoincidenciaVersionControl(cadena.Enlace[i].Direccion, "");
                    }
                }
            }
        }

        private void LeerRutasEnPlano(RespuestaOperacion respuesta, string nombre)
        {
            foreach (CadenasEncontradas cadena in cadenas)
            {
                if (!string.IsNullOrEmpty(cadena.Texto))
                {
                    var posicionFinal = cadena.Texto.Length - 1;
                    var posicionIncial = 0;
                    while (posicionIncial < posicionFinal)
                    {
                        var posicion1 = cadena.Texto.IndexOf("$/", posicionIncial);
                        if (posicion1 >= 0)
                        {
                            var posicion2 = cadena.Texto.IndexOf(" ", posicion1) == -1 ? cadena.Texto.IndexOf("\n", posicion1) : cadena.Texto.IndexOf(" ", posicion1);
                            if (posicion2 >= 0)
                            {
                                if (!cadena.Texto.Substring(posicion1, posicion2 - posicion1).ToUpper().Contains("SECUENCIAARCHIVO/SCRIPTINSTRUCCIÓN")
                                    && !cadena.Texto.Substring(posicion1, posicion2 - posicion1).ToUpper().Trim().Contains("AARCHIVO/SCRIPT"))
                                {
                                    //ja - mensaje ruta por fuera de la tabla
                                    //respuesta.Observaciones.Add($"La ruta {cadena.Texto.Substring(posicion1, posicion2 - posicion1)} de la sección '{cadena.Cadena}' debe estar contenida dentro de una tabla.");
                                    respuesta.Observaciones.Add($"El manual de instalación contiene una o mas rutas que no cumplen con el formato establecido de tablas para las rutas de TFVC.");


                                    if (!cadena.Enlace.Exists(x => x.Direccion == cadena.Texto.Substring(posicion1, posicion2 - posicion1)))
                                    {
                                        cadena.Enlace.Add(new Enlace { Direccion = cadena.Texto.Substring(posicion1, posicion2 - posicion1) });
                                    }
                                }

                                posicionIncial = posicion2;
                            }
                            else
                            {
                                posicionIncial = posicionFinal;
                            }
                        }
                        else
                        {
                            posicionIncial = posicionFinal;
                        }
                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(cadena.Cadena))
                    {
                        respuesta.Observaciones.Add($"No existe información requerida en el archivo  {nombre} seccion '{cadena.Cadena}'.");
                    }
                }
            }
        }

        private void LeerRutasfueraTabla(RespuestaOperacion respuesta)
        {
            foreach (CadenasEncontradas cadena in cadenas)
            {
                string test = "<w:Work xmlns:w='http://schemas.openxmlformats.org/wordprocessingml/2006/main'>" + cadena.XMLformat + "</w:Work>";
                XmlDocument xmltest = new XmlDocument();
                xmltest.LoadXml(test);
                XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(xmltest.NameTable);
                xmlnsManager.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                XmlNodeList nodeList = xmltest.DocumentElement.SelectNodes("/w:Work/w:r", xmlnsManager);
                foreach (XmlNode item in nodeList)
                {
                    var posicion1 = item.InnerText.IndexOf("$/");
                    if (posicion1 >= 0)
                    {
                        respuesta.Observaciones.Add($"El manual de instalación contiene una o mas rutas que no cumplen con el formato establecido de tablas para las rutas de TFVC.");
                    }
                }
            }
        }

        private void LeerRutasTabla(List<CadenasEncontradas> cadenas, RespuestaOperacion respuesta)
        {
            foreach (var cadena in cadenas)
            {
                try
                {
                    string test = "<w:Work xmlns:w='http://schemas.openxmlformats.org/wordprocessingml/2006/main'>" + cadena.XMLformat + "</w:Work>";
                    XmlDocument xmltest = new XmlDocument();
                    xmltest.LoadXml(test);
                    XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(xmltest.NameTable);
                    xmlnsManager.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                    XmlNodeList nodeList = xmltest.DocumentElement.SelectNodes("/w:Work", xmlnsManager).Item(0).ChildNodes;
                    var ruta = "";
                    var contador = 0;
                    var activo = 0;
                    var tabla = 0;
                    for (var i = 0; i < nodeList.Count; i++)
                    {
                        if (nodeList[i].Name == "w:tblGrid")
                        {
                            tabla = 1;

                        }

                        if (tabla == 1)
                        {
                            if (nodeList[i].Name == "w:tr" && (nodeList[i - 1].Name == "w:tr" || nodeList[i - 1].Name == "w:tblGrid"))
                            {
                                XmlNodeList nodosHijos = nodeList[i].SelectNodes("./w:tc/w:p", xmlnsManager);
                                for (var j = 0; j < nodosHijos.Count; j++)
                                {
                                    if (contador == 1 && (nodosHijos[j].InnerText != "Ruta" && nodosHijos[j].InnerText != "Ruta:"))
                                    {
                                        if (nodosHijos[j].InnerText.IndexOf("$/") >= 0 && j > 0 && (nodosHijos[j - 1].InnerText == "Ruta" || nodosHijos[j - 1].InnerText == "Ruta:"))
                                        {
                                            if (nodosHijos[j].InnerText.Trim().LastIndexOf("/") == nodosHijos[j].InnerText.Trim().Length - 1)
                                            {
                                                ruta = nodosHijos[j].InnerText.TrimEnd().TrimStart();
                                            }
                                            else
                                            {
                                                ruta = nodosHijos[j].InnerText.TrimEnd().TrimStart() + "/";
                                            }
                                        }

                                        if (nodosHijos[j].InnerText.IndexOf("https://dev.azure.com") >= 0 && j > 0 && (nodosHijos[j - 1].InnerText == "Ruta" || nodosHijos[j - 1].InnerText == "Ruta:"))
                                        {
                                            if (nodosHijos[j].InnerText.Trim().LastIndexOf("%") == nodosHijos[j].InnerText.Trim().Length - 1)
                                            {
                                                ruta = nodosHijos[j].InnerText.TrimEnd().TrimStart();
                                            }
                                            else
                                            {
                                                ruta = nodosHijos[j].InnerText.TrimEnd().TrimStart() + "%";
                                            }
                                        }
                                    }

                                    if (activo == 1 && nodosHijos[j].InnerText != "Ruta" && nodosHijos[j].InnerText != "Ruta:")
                                    {
                                        if (ruta != "" && (!cadena.Enlace.Exists(x => x.Direccion == ruta + nodosHijos[j].InnerText.Trim()) && j == 1))
                                        {
                                            cadena.Enlace.Add(new Enlace { Direccion = ruta + nodosHijos[j].InnerText.Trim() });
                                        }
                                        else
                                        {
                                            if (ruta == "")
                                                respuesta.Observaciones.Add($"No existe ruta para el archivo {nodosHijos[j].InnerText.Trim()} en la sección {cadena.Cadena}.");
                                        }
                                        activo = 0;
                                    }

                                    if ((nodosHijos[j].InnerText == "Ruta" || nodosHijos[j].InnerText == "Ruta:"))
                                    {
                                        contador = 1;
                                        activo = 0;

                                    }

                                    if (int.TryParse(nodosHijos[j].InnerText, out var conv))
                                    {
                                        activo = 1;
                                        contador = 0;

                                    }

                                    if ((nodosHijos[j].InnerText == ""))
                                    {
                                        if (nodeList[i - 1].Name == "w:tr")
                                        {
                                            try
                                            {
                                                XmlNodeList nodosHijos1 = nodeList[i - 1].SelectNodes("./w:tc/w:p", xmlnsManager);

                                                if (nodosHijos1[j].InnerText == "Secuencia" || int.TryParse(nodosHijos1[j].InnerText, out var conv1))
                                                {
                                                    if ((nodosHijos[j + 1].InnerText != ""))
                                                        respuesta.Observaciones.Add($"La tabla de rutas en la sección {cadena.Cadena} se encuentra mal diligenciada. No cuenta con los numeros de secuencia.");
                                                }
                                            }
                                            catch { }

                                        }

                                    }

                                }
                            }

                            if (nodeList[i].Name != "w:tblGrid" && nodeList[i].Name != "w:tr")
                            {
                                tabla = 0;
                                ruta = "";
                            }

                        }
                    }
                }
                catch (Exception) { }
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using ControlBoard.Models;

namespace ControlBoard.Domain.Commons
{
    public static class RoutingFileUtils
    {


        public static bool ExpresionCoincidenciaVersionControl(string cadena, string complementoExpresion, string formatos)
        {
            //var reg = @"[$]((\W|^)(" + complementoExpresion.Replace(@"\", @"\/") + @"))[a-zA-Z0-9\/_]+(.)(\W|^)({" + formatos + "})";
            var reg = @"[$/]+(\W|^)(" + complementoExpresion.Replace(@"\", @"\/") + @")[a-zA-Z0-9\/_]+[.]+(\w{1,10})";

            var rta = Regex.IsMatch(cadena, reg);
            return rta;
        }

        public static bool ExpresionCoincidenciaArchivos(string cadena)
        {
            var reg = @"((\W|^))[a-zA-Z0-9\/_]+(.)(\W|^)(txt|sql|xlsx|exe|msi|docx|doc|xls)";
            var rta = Regex.IsMatch(cadena, reg);
            return rta;
        }

       
        public static bool ValidarRuta(string cadena, Entidades entidad, ConfigModel config)
        {
            ////ApiTeam apitfs = new ApiTeam(config);
            //apitfs.Coleccion = entidad.RespuestaPs[6];
            //return apitfs.OnGet(cadena);
            return false;

        }

        public static string TransformacionRuta(Entidades entidades, string informacionHC)
        {
            string filepath1 = string.Empty;
            filepath1 = informacionHC.Replace(
                string.Concat("$/", entidades.RespuestaPs[4].Replace(@"\", "/")),
                entidades.RespuestaPs[5]);
            return filepath1;
        }

        public static string TransformacionRutaInverso(Entidades entidades, string informacionHC)
        {
            string filepath1 = string.Empty;
            filepath1 = informacionHC.Replace(
                entidades.RespuestaPs[5],
                string.Concat("$/", entidades.RespuestaPs[4].Replace(@"\", "/"))
                );
            return filepath1;
        }


        public static void EliminarArchivos(string path)
        {
            FileInfo info = new FileInfo(path);
            if (info.Exists)
            {
                info.Delete();
            }
        }

        public static List<string> Obtenerpath(string ruta)
        {
            var objectos = ruta.Split("/");
            var posicion = 0;
            for (var i = 0; i < objectos.Length; i++)
            {
                if (objectos[i] == "DOCUMENTACION")
                {
                    posicion = i;
                }
            }

            var pathdescarga = "";
            for (var j = 0; j < posicion; j++)
            {
                pathdescarga = pathdescarga + objectos[j] + "/";
            }

            var pathmedio = "";
            for (var j = 1; j <= posicion; j++)
            {
                pathmedio = pathmedio + objectos[j] + @"\";
            }

            var pathdescargaRecursos = pathdescarga.Replace("DOCUMENTACION", "RECURSOS");

            return new List<string> { objectos[1], pathdescarga, pathdescargaRecursos, "" };
        }

        public static string ObtenerpathGeneral(string ruta)
        {
            var objectos = ruta.Split("/");
            var posicion = 0;
            for (var i = 0; i < objectos.Length; i++)
            {
                if (objectos[i] == "FORMATOS")
                {
                    posicion = i;
                }
            }

            var pathdescarga = "";
            for (var j = 0; j < posicion; j++)
            {
                pathdescarga = pathdescarga + objectos[j] + "/";
            }

            return pathdescarga;
        }
    }
}

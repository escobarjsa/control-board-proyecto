﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using ControlBoard.Models;

namespace ControlBoard.Domain.Commons
{
    public interface IHttpUtils
    {
        Task<dynamic> Get(CredentialsModel config, string Params);
        Task<dynamic> Post(string Url, ConfigModel config, string Params);

        Task<Stream> FileDowload(CredentialsModel config, string urlFile);
    }
}

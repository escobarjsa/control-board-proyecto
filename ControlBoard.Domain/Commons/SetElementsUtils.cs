﻿using System;
using System.Collections.Generic;
using System.Text;
using ControlBoard.Models;

namespace ControlBoard.Domain.Commons
{
    public static class SetElementsUtils
    {       
        public static List<CadenasEncontradas>  GetSectionsValidation() {
            List<CadenasEncontradas> cadenas = new List<CadenasEncontradas> {
                new CadenasEncontradas { Cadena= "Procedimiento para instalación de Bases de Datos", Posicion=0 },
                new CadenasEncontradas { Cadena= "Procedimiento para instalación de Servidores", Posicion=0 },
                new CadenasEncontradas { Cadena= "Procedimiento para instalación de Estaciones", Posicion=0 },
                new CadenasEncontradas { Cadena= "Procedimiento de Rollback", Posicion=0 },
                new CadenasEncontradas { Cadena= "", Posicion=0 }//debe haber linea final
            };
            return cadenas;
        }
    }
}

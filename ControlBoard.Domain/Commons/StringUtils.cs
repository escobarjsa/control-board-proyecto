﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ControlBoard.Models;

namespace ControlBoard.Domain.Commons
{
    public static class StringUtils
    {
        public static string GetConditionalsSonar(string dataCondictions, dynamic inputs)
        {
            var conditions = dataCondictions.Split("|");
            string SonarStepResult = String.Empty;
            foreach (var condition in conditions)
            {
                JObject result = JObject.Parse(Convert.ToString(inputs));

                foreach (var x in result)
                {
                    if (x.Key.ToString().Contains(condition))
                    {
                        SonarStepResult = Convert.ToString(inputs[x.Key]);
                    }
                }

                if (!String.IsNullOrEmpty(SonarStepResult))
                {
                    return SonarStepResult;
                }
            }
            return SonarStepResult;
        }

        public static string GetNombreProjectURL(string PathURL)
        {
            var data = PathURL.Split("/");
            return data[1];
        }


        public static string TransformacionRuta(CredentialsModel credentials, string pathFuddv)
        {
            string filepath1 = string.Empty;
            filepath1 = string.Concat(credentials.AbsoluteRute, pathFuddv.Replace("$/", ""));
            return filepath1;
        }

        public static string ObtenerpathGeneral(string ruta)
        {
            var objectos = ruta.Split("/");
            var posicion = 0;
            for (var i = 0; i < objectos.Length; i++)
            {
                if (objectos[i] == "FORMATOS")
                {
                    posicion = i;
                }
            }

            var pathdescarga = "";
            for (var j = 0; j < posicion; j++)
            {
                pathdescarga = pathdescarga + objectos[j] + "/";
            }

            return pathdescarga;
        }

        public static string ObtenerpathGeneralAzure(string ruta)
        {
            var objectos = ruta.Split("%2F");
            var posicion = 0;
            for (var i = 0; i < objectos.Length; i++)
            {
                if (objectos[i] == "FORMATOS")
                {
                    posicion = i;
                }
            }

            var pathdescarga = "";
            for (var j = 0; j < posicion; j++)
            {
                pathdescarga = pathdescarga + objectos[j] + "%2F";
            }

            return pathdescarga;
        }

        public static bool ExpresionCoincidenciaVersionControl(string cadena, string complementoExpresion)
        {
            //var reg = @"[$]((\W|^)(" + complementoExpresion.Replace(@"\", @"\/") + @"))[a-zA-Z0-9\/_]+(.)(\W|^)({" + formatos + "})";
            var reg = @"[$/]+(\W|^)(" + complementoExpresion.Replace(@"\", @"\/") + @")[a-zA-Z0-9\/_]+[.]+(\w{1,10})";

            var rta = Regex.IsMatch(cadena, reg);
            return rta;
        }


    }
}

﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using ControlBoard.Models;

namespace ControlBoard.Domain.Commons
{
    public class CredentialsSettings
    {

        private readonly IConfiguration _Config;
        public CredentialsSettings(IConfiguration config)
        {
            _Config = config;
        }

        public CredentialsModel GetCredentialsServicedo()
        {
            CredentialsModel credentials = new CredentialsModel
            {
                UrlBase = _Config["Settings:Servicedo_url_api"],
                UrlBaseOriginal = _Config["Settings:Servicedo_url_api"],
                Usser = _Config["Settings:UsuarioTFS"],
                Password = _Config["Settings:PasswordTFS"],
                UrlProject = _Config["Settings:Serviceo_project_url"],
                TokenRequired = true
            };
            return credentials;
        }

        public CredentialsModel GetCredentialsGeneralAzure()
        {
            CredentialsModel credentials = new CredentialsModel
            {
                UrlBase = _Config["Settings:Devops_uri_api"],
                UrlBaseRelease = _Config["Settings:Devops_uri_api_release"],
                UrlBaseOriginal = _Config["Settings:Devops_uri_api"],
                Usser = _Config["Settings:UsuarioVSTS"],
                Password = _Config["Settings:PasswordVSTS"],
                UrlProject = _Config["Settings:Devops_project_url"],
                TokenRequired = true
            };
            return credentials;
        }

        public CredentialsModel GetCredentialsAzure(string pathFuddv)
        {
            var ProjectData = pathFuddv.Split("/");
            var pathFudd = pathFuddv.Split("path=")[1];
            var urlProject = $"{ProjectData[0]}/{ProjectData[1]}/{ProjectData[2]}/{ProjectData[3]}/{ProjectData[4]}";
            CredentialsModel credentials = new CredentialsModel
            {
                UrlBase = _Config["Settings:Devops_uri_api"],
                UrlBaseRelease = _Config["Settings:Devops_uri_api_release"],
                UrlProject = urlProject,
                Usser = _Config["Settings:UsuarioVSTS"],
                Callection = ProjectData[3],
                ProjectName = ProjectData[4],
                FuddvUrl = pathFudd,
                Password = _Config["Settings:PasswordVSTS"],
                TokenRequired = true
            };
            return credentials;
        }
 
    }
}

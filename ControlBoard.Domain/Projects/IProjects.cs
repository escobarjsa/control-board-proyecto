﻿using System;
using System.Collections.Generic;
using System.Text;
using ControlBoard.Models;

namespace ControlBoard.Domain.Projects
{
    public interface IProjects
    {
        List<ProjectEntity> GetProjects();
        void InsertProject(ProjectEntity Project);
        void SyncProjectsToDataBase();
    }
}

﻿using ControlBoard.Domain.Commons;
using ControlBoard.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace ControlBoard.Domain.Projects
{
    public class ProjectDomain
    {
        private readonly IProjects _projectsRepository;
        public readonly IConfiguration _config;
        private readonly IHttpUtils _httpCommonRepository;
        private CredentialsModel ServiceConfig;

        public ProjectDomain(IProjects project, IConfiguration config, IHttpUtils httpUtils)
        {
            _projectsRepository = project;
            _config = config;
            _httpCommonRepository = httpUtils;
            ServiceConfig = new CredentialsModel();
        }

        public ResultModel<bool> SyncProjectsToDataBase()
        {
            ResultModel<bool> result = new ResultModel<bool> { IsValid = true };
            try
            {
                GetProjectVSTS();
                //GetProjectTFS();
                result.Messagge = "Proyectos sincronizados con éxito.";
            }
            catch (Exception ex)
            {
                result.IsValid = false;
                result.Messagge = ex.Message;
            }
            return result;
        }

        public void GetProjectVSTS()
        {

            var collections = _config["Settings:CollectionVSTS"].Split(",");
            ServiceConfig.UrlBase = _config["Settings:Devops_project_url_Projects"];
            ServiceConfig.Usser = _config["Settings:UsuarioVSTS"];
            ServiceConfig.Password = _config["Settings:PasswordVSTS"];
            ServiceConfig.TokenRequired = true;

            foreach (var item in collections)
            {
                ServiceConfig.UrlBase = ServiceConfig.UrlBase.Replace("{collection}", item);
                var resultProjects = _httpCommonRepository.Get(ServiceConfig, null).Result;
                ValidationExistProject(resultProjects["value"], item);
            }

        }

        public void GetProjectTFS()
        {

            var collections = _config["Settings:CollectionTFS"].Split(",");
            ServiceConfig.UrlBase = _config["Settings:Serviceo_project_url"];
            ServiceConfig.Usser = _config["Settings:UsuarioTFS"];
            ServiceConfig.Password = _config["Settings:PasswordTFS"];
            ServiceConfig.TokenRequired = true;

            foreach (var item in collections)
            {
                ServiceConfig.UrlBase = ServiceConfig.UrlBase.Replace("{collection}", item);
                var resultProjects = _httpCommonRepository.Get(ServiceConfig, null).Result;
                ValidationExistProject(resultProjects["value"], item);
            }

        }

        public void ValidationExistProject(dynamic dataProjects, string collection)
        {
            ResultModel<bool> result = new ResultModel<bool> { IsValid = true };

            var projectdDB = _projectsRepository.GetProjects();
            foreach (var item in dataProjects)
            {
                try
                {
                    var exits = projectdDB.Where(x => x.Guid.Equals(Convert.ToString(item.id))).FirstOrDefault();
                    if (exits == null)
                    {
                        ProjectEntity project = new ProjectEntity
                        {
                            Nombre = item.name,
                            Descripcion = item.description,
                            Coleccion = collection,
                            Guid = item.id,
                            CreatedOn = DateTime.Now
                        };
                        _projectsRepository.InsertProject(project);
                    }
                }
                catch (Exception ex)
                {
                    result.IsValid = false;
                    result.Messagge = ex.Message;
                }
            }
        }
    }
}

# Introduction 
TODO: Give a short introduction of your project. Let this section explain the objectives or the motivation behind this project. 

# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
2.	Software dependencies
3.	Latest releases
4.	API references

# Build and Test
TODO: Describe and show how to build your code and run the tests. 

# Contribute
TODO: Explain how other users and developers can contribute to make your code better. 

If you want to learn more about creating good readme files then refer the following [guidelines](https://docs.microsoft.com/en-us/azure/devops/repos/git/create-a-readme?view=azure-devops). You can also seek inspiration from the below readme files:
- [ASP.NET Core](https://github.com/aspnet/Home)
- [Visual Studio Code](https://github.com/Microsoft/vscode)
- [Chakra Core](https://github.com/Microsoft/ChakraCore)

## Imagen docker
### Control Board
##### Build Control Board

Para compilar la aplicación, hay que posicionarnos en la carpeta **control-board**, estando allí, ejecutar el comando
`docker build --no-cache -f control-board.dockerfile -t control-board:latest .`
Esto con el fin de compilar la aplicación en docker.

##### Ejecutar Contral Board

Para ejecutar la aplicación, hay que ejecutar el comando `docker run --name control-board --rm -it -p 8080:80 control-board:latest` Esto con el fin de crear el contenedor a partir de la imagen que se le indica y además poner el contenedor en funcionamiento.

La aplicación se ejecuta en http://localhost:8080/ para acceder a la aplicación una ver este en ejecución y probar los End Point's http://localhost:8080/swagger/

##### Ejecutar Control Board con Docker Compose

Para ejecutar la aplicación con docker compose; hay que posicionarse en la ruta raiz del proyecto, donde se encuentra el docker compose "docker-compose.yml", estando allí ejecutar el comando `docker-compose up`

La aplicacion se ejecuta en http://localhost:5001/ ara acceder a la aplicación una ver este en ejecución y probar los End Point's http://localhost:5001/swagger/
